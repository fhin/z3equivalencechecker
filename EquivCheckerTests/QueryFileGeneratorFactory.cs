﻿using EquivCheckerConsole;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace EquivCheckerTests
{
    public class QueryFileGeneratorFactory
    {

        private static readonly string TMP_PATH = Path.GetTempPath();
        private static readonly string STD_EXTENSION = ".blif";
        private static readonly int DEFAULT_BUFFER_SIZE = 1024;
        public static readonly string DEFAULT_MODEL_NAME = "default_model_name";

        private static readonly string[] defaultPrimaryInputs = new string[3] { "a", "b", "c" };
        private static readonly string[] defaultPrimaryOutputs = new string[3] { "d", "e", "f" };

        private static readonly IList<(string[], string)> defaultLogicGates = new List<(string[], string)>()
        {
            (new string[2] {"a", "b"}, "d"), (new string[2] {"a", "c"}, "e"), (new string[2] {"b", "c"}, "f")
        };

        private static readonly IList<(string[], string)> defaultLogicGatesOneLevelDependency = new List<(string[], string)>()
        {
              (new string[2] {"a", "f"}, "d"), (new string[2] {"b", "c"}, "f")
        };

        private static readonly IList<(string[], string)> defaultLogicGatesTwoLevelDependency = new List<(string[], string)>()
        {
              (new string[2] {"e", "f"}, "d"), (new string[2] {"a", "f"}, "e"), (new string[2] {"b", "c"}, "f")
        };

        private static readonly IList<(string[], string)> defaultLogicGatesMixedPrimaryandGeneralIOs = new List<(string[], string)>()
        {
            (new string[2] {"a", "b"}, "d"), (new string[2] {"g", "h"}, "i"),
            (new string[2] {"d", "i"}, "k"), (new string[2] {"k", "i"}, "e")
        };

        public static string CreateFileOneLineInputsAndOutputs(bool useDuplicateInputs, bool useDuplicateOutputs)
        {
            string fileName = "CreateFileOneLineInputsAndOutputs";
            string content = CreateModelHeaderContent(defaultPrimaryInputs, defaultPrimaryOutputs, useDuplicateInputs, useDuplicateOutputs);
            content += AddDefaultLogicGate(defaultPrimaryInputs[0], defaultPrimaryOutputs[0]);
            return CreateFile(fileName, content);
        }

        public static string CreateFileMultipleLinesInputsAndOutputs(int numInputLines, int numOutputLines,
            bool useDuplicateInputs, bool useDuplicateOutputs)
        {

            string fileName = "CreateFileMultipleLinesInputsAndOutputsDuplicates";
            return CreateFileMultipleLinesInputsAndOutputs(fileName, numInputLines, numOutputLines, false,
                useDuplicateInputs, useDuplicateOutputs);
        }

        public static string CreateFileMultipleLinesInputsAndOutputs(int numInputLines, int numOutputLines)
        {
            string fileName = "CreateFileMultipleLinesInputsAndOutputs";
            return CreateFileMultipleLinesInputsAndOutputs(fileName, numInputLines, numOutputLines, false,
                false, false);
        }

        public static string CreateFileConcatinatedInputsandOutputs(int numInputLines, int numOutputLines,
         bool useDuplicateInputs, bool useDuplicateOutputs)
        {
            string fileName = "CreateFileConcatinatedInputsandOutputsDuplicates";
            return CreateFileMultipleLinesInputsAndOutputs(fileName, numInputLines, numOutputLines, true,
                useDuplicateInputs, useDuplicateOutputs);
        }

        public static string CreateFileConcatinatedInputsandOutputs(int numInputLines, int numOutputLines)
        {
            string fileName = "CreateFileConcatinatedInputsandOutputs";
            return CreateFileMultipleLinesInputsAndOutputs(fileName, numInputLines, numOutputLines, true,
                false, false);
        }

        public static string CreateFilesWithIOsMultipleLogicGatesNoDependencies(bool useOneInputPerRow)
        {
            string fileName = "CreateFilesWithIOsMultipleLogicGatesNoDependencies";
            string content = CreateModelHeaderContent(defaultPrimaryInputs, defaultPrimaryOutputs) + "\n";
            foreach ((string[] inputs, string output) in defaultLogicGates)
            {
                content += AddLogicGate(inputs, output, useOneInputPerRow, false);
            }
            return CreateFile(fileName, content);
        }

        public static string CreateFileNoIOsMultipleLogicGatesNoDependencies()
        {
            string fileName = "CreateFileNoIOsMultipleLogicGatesNoDependencies";
            string content = ".model " + DEFAULT_MODEL_NAME + "\n";
            foreach ((string[] inputs, string output) in defaultLogicGates)
            {
                content += AddLogicGate(inputs, output, false, false);
            }
            return CreateFile(fileName, content);
        }

        public static string CreateFileMixedPrimaryAndGeneralIOs()
        {
            string fileName = "CreateFileMixedPrimaryAndGeneralIOs";
            string content = CreateModelHeaderContent(defaultPrimaryInputs, defaultPrimaryOutputs) + "\n";
            foreach ((string[] inputs, string output) in defaultLogicGatesMixedPrimaryandGeneralIOs)
            {
                content += AddLogicGate(inputs, output, false, false);
            }
            return CreateFile(fileName, content);
        }

        public static string CreateFileMultipleLogicGatesWithDependencies(int dependencyLevel)
        {
            if (dependencyLevel > 2 || dependencyLevel < 1)
            {
                dependencyLevel = 1;
            }
            string fileName = "CreateFileMultipleLogicGatesWithDependencies";
            string fileContent = ".model " + DEFAULT_MODEL_NAME + "\n";
            IList<(string[], string)> logicGatesToUse;
            if (dependencyLevel == 1)
            {
                logicGatesToUse = defaultLogicGatesOneLevelDependency;
            }
            else
            {
                logicGatesToUse = defaultLogicGatesTwoLevelDependency;
            }
            foreach ((string[] inputs, string output) in logicGatesToUse)
            {
                fileContent += AddLogicGate(inputs, output, false, false);
            }
            return CreateFile(fileName, fileContent);
        }

        private static string AddLogicGate(IEnumerable<string> inputs, string output, bool useOneInputPerRow, bool negatedOutput)
        {
            string logicGateContent = ".names ";
            logicGateContent += string.Join(" ", inputs) + " " + output + "\n";
            StringBuilder builder = new StringBuilder();
            int numInputs = inputs.Count();
            if (useOneInputPerRow)
            {
                for (int i = 0; i < numInputs; i++)
                {
                    StringBuilder lineDataBuilder = Utils.RepeatAsMutuable("-", numInputs);
                    lineDataBuilder[i] = '1';
                    lineDataBuilder.Append(" " + (negatedOutput ? "0" : "1"));
                    builder.AppendLine(lineDataBuilder.ToString());
                }
            }
            else
            {
                string lineData = Utils.Repeat("1", numInputs) + " " + (negatedOutput ? "0" : "1");
                builder.AppendLine(lineData);
            }
            logicGateContent += builder.ToString();
            return logicGateContent;
        }

        private static string CreateFileMultipleLinesInputsAndOutputs(string fileName, int numInputLines, int numOutputLines,
            bool useConcat, bool useDuplicateInputs, bool useDuplicateOutputs)
        {
            if (numInputLines < 0)
            {
                numInputLines = 0;
            }
            if (numOutputLines < 0)
            {
                numOutputLines = 0;
            }
            IList<string[]> primaryInputs = new List<string[]>();
            for (int i = 0; i < numInputLines; i++)
            {
                string[] pInputs = new string[defaultPrimaryInputs.Length];
                for (int j = 0; j < defaultPrimaryInputs.Length; j++)
                {
                    pInputs[j] = defaultPrimaryInputs[j] + i + j;
                }
                primaryInputs.Add(pInputs);
            }
            if (useDuplicateInputs)
            {
                var lastRowIdx = numInputLines > 1 ? numInputLines - 1 : 0;
                (primaryInputs[lastRowIdx])[defaultPrimaryInputs.Length - 1] = primaryInputs.First().First();
            }
            IList<string[]> primaryOutputs = new List<string[]>();
            for (int i = 0; i < numOutputLines; i++)
            {
                string[] pOutputs = new string[defaultPrimaryOutputs.Length];
                for (int j = 0; j < defaultPrimaryOutputs.Length; j++)
                {
                    pOutputs[j] = defaultPrimaryOutputs[j] + i + j;
                }
                primaryOutputs.Add(pOutputs);
            }
            if (useDuplicateOutputs)
            {
                var lastRowIdx = numOutputLines > 1 ? numOutputLines - 1 : 0;
                (primaryOutputs[lastRowIdx])[defaultPrimaryOutputs.Length - 1] = primaryOutputs.First().First();
            }
            string content = CreateModelHeaderContent(primaryInputs, primaryOutputs, useConcat);
            content += AddDefaultLogicGate(primaryInputs[0][0], primaryOutputs[0][0]);
            return CreateFile(fileName, content);
        }

        private static string CreateModelHeaderContent(IEnumerable<string> primaryInputs, IEnumerable<string> primaryOutputs,
            bool useDuplicateInput, bool useDuplicateOutput)
        {
            if (useDuplicateInput)
            {
                var tmpPInputs = primaryInputs.ToArray();
                tmpPInputs[0] = tmpPInputs[tmpPInputs.Length - 1];
                primaryInputs = tmpPInputs.AsEnumerable();
            }
            if (useDuplicateOutput)
            {
                var tmpPOutputs = primaryOutputs.ToArray();
                tmpPOutputs[0] = tmpPOutputs[tmpPOutputs.Length - 1];
                primaryOutputs = tmpPOutputs.AsEnumerable();
            }
            return CreateModelHeaderContent(primaryInputs, primaryOutputs);
        }

        private static string CreateModelHeaderContent(IList<string[]> primaryInputLines
            , IList<string[]> primaryOutputLines, bool useConcat)
        {
            string headerContent = ".model " + DEFAULT_MODEL_NAME + "\n";
            if (primaryInputLines.Any())
            {
                for (int i = 0; i < primaryInputLines.Count(); i++)
                {
                    var primaryInputs = primaryInputLines[i];
                    if (useConcat && i > 0)
                    {
                        headerContent += string.Join(" ", primaryInputs) + (i + 1 < primaryInputLines.Count ? " \\" : "") + "\n";
                    }
                    else
                    {
                        headerContent += ".inputs " + string.Join(" ", primaryInputs) + (useConcat ? " \\" : "") + "\n";
                    }
                }
            }
            if (primaryOutputLines.Any())
            {
                for (int i = 0; i < primaryOutputLines.Count(); i++)
                {
                    var primaryOutputs = primaryOutputLines[i];
                    if (useConcat && i > 0)
                    {
                        headerContent += string.Join(" ", primaryOutputs) + (i + 1 < primaryOutputLines.Count ? " \\" : "") + "\n";
                    }
                    else
                    {
                        headerContent += ".outputs " + string.Join(" ", primaryOutputs) + (useConcat ? " \\" : "") +
                            (i + 1 < primaryOutputLines.Count ? "\n" : "");
                    }
                }
            }
            return headerContent;
        }

        private static string CreateModelHeaderContent(IEnumerable<string> primaryInputs, IEnumerable<string> primaryOutputs)
        {
            string headerContent = ".model " + DEFAULT_MODEL_NAME + "\n";
            if (primaryInputs.Any())
            {
                headerContent += ".inputs " + string.Join(" ", primaryInputs) + "\n";
            }
            if (primaryOutputs.Any())
            {
                headerContent += ".outputs " + string.Join(" ", primaryOutputs);
            }
            return headerContent;
        }

        private static string AddDefaultLogicGate(string input, string output)
        {
            return "\n.names " + input + " " + output + "\n1 1";
        }

        private static string CreateFile(string fileName, string content)
        {
            string filePath = Path.Combine(TMP_PATH, fileName + STD_EXTENSION);
            using (FileStream fileStream = File.Create(filePath, DEFAULT_BUFFER_SIZE, FileOptions.SequentialScan))
            {
                using (StreamWriter writer = new StreamWriter(fileStream))
                {
                    writer.Write(content);
                    writer.Flush();
                }
            }
            return filePath;
        }
    }
}
