﻿using EquivCheckerCompGen;
using EquivCheckerConsole;
using Microsoft.Z3;
using System;
using System.Collections.Generic;
using Xunit;

namespace EquivCheckerTests
{
    public class EquivCheckerTests : IDisposable
    {
        UserQueryParser parser;
        Context context;
        object lockObj;
        EquivChecker checker;

        public EquivCheckerTests()
        {
            parser = new UserQueryParser();
            context = new Microsoft.Z3.Context();
            lockObj = new object();
            checker = new EquivChecker(context, lockObj);
        }

        public void Dispose()
        {
            context.Dispose();
        }

        [Fact]
        public async System.Threading.Tasks.Task CheckMiterExceptionModelsDifferentNumPrimaryOutputs()
        {
            var path_model_1 = @"Resources/simple.blif";
            var path_model_2 = @"Resources/con1.blif";

            var query = Utils.ReadFromFile(path_model_1).GetAwaiter().GetResult();
            parser.ParseQuery(query);
            var model_1 = parser.Models[0];
            query = Utils.ReadFromFile(path_model_2).GetAwaiter().GetResult();
            parser.ParseQuery(query);
            var model_2 = parser.Models[0];
            await Assert.ThrowsAsync<Exception>(() => EquivChecker.CreateMiter(context, model_1, model_2));

        }

        [Fact]
        public async System.Threading.Tasks.Task CheckSATExceptionModelsDifferentNumPrimaryOutputs()
        {
            var path_model_1 = @"Resources/simple.blif";
            var path_model_2 = @"Resources/con1.blif";
            await Assert.ThrowsAsync<Exception>(() => EquivChecker.CheckSAT(path_model_1, path_model_2, false, false, false));

        }

        [Fact]
        public void CheckSimpleModel()
        {
            var query = Utils.ReadFromFile(@"Resources/simple.blif").GetAwaiter().GetResult();
            parser.ParseQuery(query);
            var model = parser.Models[0];
            model.Name = "";
            checker.CreateExpressionsForModel(model);
            var expectedBoolExprsDict = new Dictionary<string, BoolExpr>
            {
                { "a", context.MkBoolConst("a") },
                { "b", context.MkBoolConst("b") }
            };
            expectedBoolExprsDict.Add("c", context.MkAnd(expectedBoolExprsDict["a"], expectedBoolExprsDict["b"]));

            BoolExpr actualExpr;
            foreach (var expBoolExprKVPair in expectedBoolExprsDict)
            {
                Assert.True(checker.LiteralExprLookupDict.TryGetValue(expBoolExprKVPair.Key, out actualExpr));
                Assert.Equal(expBoolExprKVPair.Value, actualExpr);
            }
        }

        [Fact]
        public void CheckCon1Model()
        {
            var query = Utils.ReadFromFile(@"Resources/con1.blif").GetAwaiter().GetResult();
            parser.ParseQuery(query);
            var model = parser.Models[0];
            model.Name = "";
            checker.CreateExpressionsForModel(model);
            var expectedBoolExprsDict = new Dictionary<string, BoolExpr>
            {
                { "v0", context.MkBoolConst("v0") }, { "v1", context.MkBoolConst("v1") }, { "v2", context.MkBoolConst("v2") },
                { "v3", context.MkBoolConst("v3") }, { "v4", context.MkBoolConst("v4") }, { "v5", context.MkBoolConst("v5") },
                { "v6", context.MkBoolConst("v6") }
            };
            expectedBoolExprsDict.Add("[227]", context.MkNot(expectedBoolExprsDict["v1"]));
            expectedBoolExprsDict.Add("[228]", context.MkNot(expectedBoolExprsDict["v2"]));
            expectedBoolExprsDict.Add("[229]", context.MkNot(expectedBoolExprsDict["v0"]));
            expectedBoolExprsDict.Add("[13]", context.MkOr(new List<BoolExpr>() {
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[227]"]), context.MkNot(expectedBoolExprsDict["v2"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[228]"]), context.MkNot(expectedBoolExprsDict["v2"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[227]"]), context.MkNot(expectedBoolExprsDict["v0"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[228]"]), context.MkNot(expectedBoolExprsDict["v0"]))
            }));
            expectedBoolExprsDict.Add("[226]", context.MkNot(expectedBoolExprsDict["v3"]));
            expectedBoolExprsDict.Add("[231]", context.MkNot(expectedBoolExprsDict["v4"]));
            expectedBoolExprsDict.Add("[14]", context.MkOr(new List<BoolExpr>()
            {
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[229]"]), context.MkNot(expectedBoolExprsDict["v4"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["v5"]), context.MkNot(expectedBoolExprsDict["v4"]))
            }));
            expectedBoolExprsDict.Add("v7.0", context.MkOr(new List<BoolExpr>()
            {
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[13]"]), context.MkNot(expectedBoolExprsDict["[226]"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[14]"]), context.MkNot(expectedBoolExprsDict["[227]"]))
            }));
            expectedBoolExprsDict.Add("[267]", context.MkOr(new List<BoolExpr>()
            {
                context.MkNot(expectedBoolExprsDict["v6"]),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[227]"]), context.MkNot(expectedBoolExprsDict["[231]"]))
            }));
            expectedBoolExprsDict.Add("[269]", context.MkOr(new List<BoolExpr>()
            {
                context.MkNot(expectedBoolExprsDict["v4"]),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["v1"]), context.MkNot(expectedBoolExprsDict["v3"]))
            }));
            expectedBoolExprsDict.Add("[195]", context.MkOr(new List<BoolExpr>()
            {
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[229]"]), context.MkNot(expectedBoolExprsDict["[269]"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[267]"]), context.MkNot(expectedBoolExprsDict["[269]"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[229]"]), context.MkNot(expectedBoolExprsDict["v0"])),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["[267]"]), context.MkNot(expectedBoolExprsDict["v0"]))
            }));
            expectedBoolExprsDict.Add("v7.1", context.MkOr(new List<BoolExpr>()
            {
                context.MkNot(expectedBoolExprsDict["[195]"]),
                context.MkAnd(context.MkNot(expectedBoolExprsDict["v1"]), context.MkNot(expectedBoolExprsDict["v4"]))
            }));
            BoolExpr actualExpr;
            foreach (var expBoolExprKVPair in expectedBoolExprsDict)
            {
                Assert.True(checker.LiteralExprLookupDict.TryGetValue(expBoolExprKVPair.Key, out actualExpr));
                Assert.Equal(expBoolExprKVPair.Value, actualExpr);
            }
        }
    }
}
