using EquivCheckerCompGen;
using EquivCheckerCompGen.Entities;
using EquivCheckerConsole;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using Xunit;

namespace EquivCheckerTests
{
    public class QueryParsingTests
    {
        readonly UserQueryParser parser;

        public QueryParsingTests()
        {
            parser = new UserQueryParser();
        }

        [Fact]
        public void OnlyOneLineIOsNoLogicGates()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[3] { "d", "e", "f" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFileOneLineInputsAndOutputs(false, false);

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void MultipleLinesOfIOsNoLogicGates()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            int numInputLines = 3;
            int numOutputLines = 3;
            string[] expectedPrimaryInputs = new string[] { "a00", "b01", "c02", "a10", "b11", "c12", "a20", "b21", "c22" };
            string[] expectedPrimaryOutputs = new string[] { "d00", "e01", "f02", "d10", "e11", "f12", "d20", "e21", "f22" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFileMultipleLinesInputsAndOutputs(numInputLines, numOutputLines);

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void ConcatinatedLinesOfIOsNoLogicGates()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            int numInputLines = 3;
            int numOutputLines = 3;
            string[] expectedPrimaryInputs = new string[] { "a00", "b01", "c02", "a10", "b11", "c12", "a20", "b21", "c22" };
            string[] expectedPrimaryOutputs = new string[] { "d00", "e01", "f02", "d10", "e11", "f12", "d20", "e21", "f22" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFileConcatinatedInputsandOutputs(numInputLines, numOutputLines);

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void DuplicateInputEntriesNoLogicGates()
        {
            string filePath = QueryFileGeneratorFactory.CreateFileOneLineInputsAndOutputs(true, false);

            string expectedExceptionMessage = ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_INPUT, "c");
            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                parser.RethrowExceptionsAfterLogging();
                Exception thrownException = Assert.Throws<Exception>(() => parser.ParseQuery(query));
                Assert.Equal(expectedExceptionMessage, thrownException.Message);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void DuplicateOutputEntriesNoLogicGates()
        {
            string filePath = QueryFileGeneratorFactory.CreateFileOneLineInputsAndOutputs(false, true);

            string expectedExceptionMessage = ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_OUTPUT, "f");
            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                parser.RethrowExceptionsAfterLogging();
                Exception thrownException = Assert.Throws<Exception>(() => parser.ParseQuery(query));
                Assert.Equal(expectedExceptionMessage, thrownException.Message);
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void OnlyOneLineIOsWithLogicGatesUseOnInputPerRow()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[3] { "d", "e", "f" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFilesWithIOsMultipleLogicGatesNoDependencies(true);

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"a", "b"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[0], outputMappings["d"]),
                    CreateOutputCover(new int[] { 1 }, new int[0], outputMappings["d"])
                }),
                CreateLogicGate(new string[] {"a", "c"}, "e", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[0], outputMappings["e"]),
                    CreateOutputCover(new int[] { 1 }, new int[0], outputMappings["e"])
                }),
                CreateLogicGate(new string[] {"b", "c"}, "f", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[0], outputMappings["f"]),
                    CreateOutputCover(new int[] { 1 }, new int[0], outputMappings["f"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.True(CompareLogicGates(expectedLg, actualLg));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void OnlyOneLineIOsWithLogicGatesUseAllInputs()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[3] { "d", "e", "f" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFilesWithIOsMultipleLogicGatesNoDependencies(false);

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"a", "b"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["d"])
                }),
                CreateLogicGate(new string[] {"a", "c"}, "e", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["e"])
                }),
                CreateLogicGate(new string[] {"b", "c"}, "f", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["f"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.True(CompareLogicGates(expectedLg, actualLg));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void NoIOsWithLogicGates()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[3] { "d", "e", "f" };
            string[] expectedGeneralInputs = expectedPrimaryInputs;
            string[] expectedGeneralOutputs = expectedPrimaryOutputs;
            string filePath = QueryFileGeneratorFactory.CreateFileNoIOsMultipleLogicGatesNoDependencies();

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"a", "b"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["d"])
                }),
                CreateLogicGate(new string[] {"a", "c"}, "e", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["e"])
                }),
                CreateLogicGate(new string[] {"b", "c"}, "f", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["f"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.True(CompareLogicGates(expectedLg, actualLg));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void MultipleLogicGatesWithMixedPrimaryAndGeneralInputs()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[5] { "a", "b", "c", "g", "h" };
            string[] expectedPrimaryOutputs = new string[3] { "d", "e", "f" };
            string[] expectedGeneralInputs = new string[8] { "a", "b", "c", "g", "h", "d", "i", "k" };
            string[] expectedGeneralOutputs = new string[5] { "d", "e", "f", "i", "k" };
            string filePath = QueryFileGeneratorFactory.CreateFileMixedPrimaryAndGeneralIOs();

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"a", "b"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["d"])
                }),
                CreateLogicGate(new string[] {"g", "h"}, "i", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["i"])
                }),
                CreateLogicGate(new string[] {"d", "i"}, "k", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["k"])
                }),
                CreateLogicGate(new string[] {"k", "i"}, "e", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["e"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.Contains(actualModel.LogicGates, actualLogicGate => CompareLogicGatesNoAsserts(expectedLg, actualLogicGate));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void NoIOsWithLogicGatesOneLevelDependency()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[1] { "d" };
            string[] expectedGeneralInputs = new string[4] { "b", "c", "a", "f" };
            string[] expectedGeneralOutputs = new string[2] { "f", "d" };
            string filePath = QueryFileGeneratorFactory.CreateFileMultipleLogicGatesWithDependencies(1);

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"b", "c"}, "f", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["f"])
                }),
                CreateLogicGate(new string[] {"a", "f"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["d"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                Assert.Equal(expectedPrimaryInputs, actualModel.PrimaryInputs);
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.True(CompareLogicGates(expectedLg, actualLg));
                    //Assert.Contains(actualModel.LogicGates, actualLogicGate => CompareLogicGatesNoAsserts(expectedLg, actualLogicGate));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void UseIOsWithLogicGatesTwoLevelDependency()
        {
            string expectedModelName = QueryFileGeneratorFactory.DEFAULT_MODEL_NAME;
            string[] expectedPrimaryInputs = new string[3] { "a", "b", "c" };
            string[] expectedPrimaryOutputs = new string[1] { "d" };
            string[] expectedGeneralInputs = new string[5] { "b", "c", "a", "f", "e" };
            string[] expectedGeneralOutputs = new string[3] { "f", "e", "d" };
            string filePath = QueryFileGeneratorFactory.CreateFileMultipleLogicGatesWithDependencies(2);

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"b", "c"}, "f", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["f"])
                }),
                CreateLogicGate(new string[] {"a", "f"}, "e", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["e"])
                }),
                CreateLogicGate(new string[] {"e", "f"}, "d", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[0], outputMappings["d"])
                })
            };

            Exception exception = null;
            try
            {
                var query = Utils.ReadFromFile(filePath).GetAwaiter().GetResult();
                Console.WriteLine(query);
                parser.ParseQuery(query);

                Assert.NotNull(parser.Models);
                Assert.Single(parser.Models);
                Model actualModel = parser.Models[0];
                Assert.Equal(expectedModelName, actualModel.Name);
                foreach (var pInput in expectedPrimaryInputs)
                {
                    Assert.Contains(actualModel.PrimaryInputs, actualPInput => actualPInput.Equals(pInput));
                }
                Assert.Equal(expectedPrimaryOutputs, actualModel.PrimaryOutputs);

                var generalInputsKVMap = actualModel.GeneralInputs.ToList();
                generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

                var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
                generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

                var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
                Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

                Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
                for (int i = 0; i < expectedLogicGates.Count; i++)
                {
                    var expectedLg = expectedLogicGates[i];
                    var actualLg = actualModel.LogicGates[i];
                    Assert.True(CompareLogicGates(expectedLg, actualLg));
                }
            }
            catch (Exception e)
            {
                exception = e;
            }
            finally
            {
                File.Delete(filePath);
                Assert.False(File.Exists(filePath));
            }
            if (exception != null) throw exception;
        }

        [Fact]
        public void Simple_Model_Test()
        {
            string expectedModelName = "simple";
            string[] expectedInputs = new string[] { "a", "b" };
            string[] expectedOutputs = new string[] { "c" };
            string[] expectedGeneralInputs = new string[] { "a", "b" };
            string[] expectedGeneralOutputs = new string[] { "c" };

            SingleOutputCover soc = new SingleOutputCover();
            soc.AddInput(0, 0, false);
            soc.AddInput(1, 1, false);
            soc.SetOutput(0);
            LogicGate expectedLogicGate = new LogicGate()
            {
                Socs = new List<SingleOutputCover>() { soc },
                Inputs = expectedInputs,
                Output = expectedOutputs[0]
            };
            int[] expectedInputsIdx = new int[] { 0, 1 };

            var query = Utils.ReadFromFile(@"Resources/simple.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            Assert.Single(parser.Models);
            Model actualModel = parser.Models[0];
            Assert.Equal(expectedModelName, actualModel.Name);
            Assert.Equal(expectedInputs, actualModel.PrimaryInputs);
            Assert.Equal(expectedOutputs, actualModel.PrimaryOutputs);

            var generalInputsKVMap = actualModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

            var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

            Assert.NotNull(actualModel.LogicGates);
            Assert.Single(actualModel.LogicGates);
            var actualLogicGate = actualModel.LogicGates[0];
            Assert.Equal(expectedOutputs[0], actualLogicGate.Output);
            Assert.Equal(expectedInputs, actualLogicGate.Inputs);
            Assert.NotNull(actualLogicGate.Socs);
            Assert.Single(actualLogicGate.Socs);
            var actualSingleOutputCover = actualLogicGate.Socs[0];

            Assert.Empty(actualSingleOutputCover.NegatedInputsSet);
            Assert.Equal(0, actualSingleOutputCover.OutputIdx);
            Assert.Equal(expectedInputsIdx, actualSingleOutputCover.InputIdxs);
        }

        [Fact]
        public void ConcatinatedIOs()
        {
            string expectedModelName = "concatModel";
            string[] expectedInputs = new string[] { "a", "b", "c", "d" };
            string[] expectedOutputs = new string[] { "e", "f", "g", "h" };
            string[] expectedGeneralInputs = expectedInputs;
            string[] expectedGeneralOutputs = expectedOutputs;

            var query = Utils.ReadFromFile(@"Resources/concat_model.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            Assert.Single(parser.Models);
            Model actualModel = parser.Models[0];
            Assert.Equal(expectedModelName, actualModel.Name);
            Assert.Equal(expectedInputs, actualModel.PrimaryInputs);
            Assert.Equal(expectedOutputs, actualModel.PrimaryOutputs);

            var generalInputsKVMap = actualModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

            var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);

            Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);
            Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);
            Assert.Empty(actualModel.LogicGates);
        }

        [Fact]
        public void Con1_Model_Test()
        {
            string expectedModelName = "new_con1.pla";
            string[] expectedInputs = new string[] { "v0", "v1", "v2", "v3", "v4", "v5", "v6" };
            string[] expectedOutputs = new string[] { "v7.0", "v7.1" };
            /*
            string[] expectedGeneralInputs = new string[]
            {
                "v0", "v1", "v2", "v3", "v4", "v5", "v6",
                "[13]", "[226]", "[14]", "[227]",
                "[195]", "[228]", "[229]",
                "[231]", "[267]", "[269]"
            };
            */
            string[] expectedGeneralInputs = new string[]
            {
                "v0", "v1", "v2", "v3", "v4", "v5", "v6",
                "[227]", "[228]", "[229]", "[13]", "[226]",
                "[14]", "[231]", "[267]", "[269]", "[195]"
            };
            Dictionary<string, int> inputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var input in expectedGeneralInputs)
            {
                inputMappings.Add(input, counter++);
            }
            string[] expectedGeneralOutputs = new string[]
            {
                "v7.0", "v7.1", "[227]", "[228]", "[229]", "[13]",
                "[226]", "[231]", "[14]", "[267]", "[269]", "[195]"
            };

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            var query = Utils.ReadFromFile(@"Resources/con1.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            Assert.Single(parser.Models);
            Model actualModel = parser.Models[0];
            Assert.Equal(expectedModelName, actualModel.Name);
            Assert.Equal(expectedInputs, actualModel.PrimaryInputs);
            Assert.Equal(expectedOutputs, actualModel.PrimaryOutputs);

            var generalInputsKVMap = actualModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

            var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"v1"}, "[227]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[227]"])
                }),
                CreateLogicGate(new string[] {"v2"}, "[228]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[228]"])
                }),
                CreateLogicGate(new string[] {"v0"}, "[229]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[229]"])
                }),
                CreateLogicGate(new string[] {"[227]", "[228]", "v2", "v0"}, "[13]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 0,3 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 1,3 }, new int[]{ 0, 1 }, outputMappings["[13]"])
                }),
                CreateLogicGate(new string[] {"v3"}, "[226]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[226]"])
                }),
                CreateLogicGate(new string[] {"v4"}, "[231]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[231]"])
                }),
                CreateLogicGate(new string[] {"[229]","v5", "v4"}, "[14]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0,1 }, outputMappings["[14]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0,1 }, outputMappings["[14]"])
                }),
                CreateLogicGate(new string[] {"[13]","[226]", "[14]","[227]"}, "v7.0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["v7.0"]),
                    CreateOutputCover(new int[] { 2,3 }, new int[]{ 0,1 }, outputMappings["v7.0"])
                }),
                CreateLogicGate(new string[] {"[227]","[231]", "v6"}, "[267]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["[267]"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["[267]"])
                }),
                CreateLogicGate(new string[] {"v1","v3", "v4"}, "[269]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["[269]"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["[269]"])
                }),
                CreateLogicGate(new string[] {"[229]","[267]", "[269]","v0"}, "[195]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 0,3 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 1,3 }, new int[]{ 0,1 }, outputMappings["[195]"])
                }),
                CreateLogicGate(new string[] {"v1","v4","[195]"}, "v7.1", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["v7.1"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["v7.1"])
                })
            };
            Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
            for (int i = 0; i < expectedLogicGates.Count; i++)
            {
                var expectedLg = expectedLogicGates[i];
                var actualLg = actualModel.LogicGates[i];
                Assert.True(CompareLogicGates(expectedLg, actualLg));
            }
        }

        [Fact]
        public void No_Optional_Parts_Test()
        {
            string expectedModelName = "NONE";
            string[] expectedInputs = new string[] { "v0", "v1", "v2", "v3", "v4", "v5", "v6" };
            string[] expectedOutputs = new string[] { "v7.0", "v7.1" };
            string[] expectedGeneralInputs = new string[]
            {
                "v0", "v1", "v2", "v3", "v4", "v5", "v6",
                "[13]", "[226]", "[14]", "[227]",
                "[195]", "[228]", "[229]",
                "[231]", "[267]", "[269]"
            };
            Dictionary<string, int> inputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var input in expectedGeneralInputs)
            {
                inputMappings.Add(input, counter++);
            }
            /*
            string[] expectedGeneralOutputs = new string[]
            {
                "v7.0", "v7.1", "[227]", "[228]", "[229]", "[13]",
                "[226]", "[231]", "[14]", "[267]", "[269]", "[195]"
            };
            */
            string[] expectedGeneralOutputs = new string[]
            {
                "v7.0", "v7.1", "[227]", "[228]", "[229]", "[13]",
                "[226]", "[231]", "[14]", "[267]", "[269]", "[195]"
            };

            var query = Utils.ReadFromFile(@"Resources/no_optional_parts.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            Assert.Single(parser.Models);
            Model actualModel = parser.Models[0];
            Assert.Equal(expectedModelName, actualModel.Name);
            var actualPInputsSet = actualModel.PrimaryInputs.ToHashSet();
            Assert.False(expectedInputs.Where(input => !actualPInputsSet.Contains(input)).Any());
            var actualPOutputsSet = actualModel.PrimaryOutputs.ToHashSet();
            Assert.False(expectedOutputs.Where(output => !actualPOutputsSet.Contains(output)).Any());

            var actualGInputsSet = actualModel.GeneralInputs.Keys.ToHashSet();
            Assert.False(expectedGeneralInputs.Where(gInput => !actualGInputsSet.Contains(gInput)).Any());

            var actualGOutputsSet = actualModel.GeneralOutputs.Keys.ToHashSet();
            Assert.False(expectedGeneralOutputs.Where(gOutput => !actualGOutputsSet.Contains(gOutput)).Any());

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            counter = 0;
            foreach (var output in actualGOutputsSet)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new string[] {"v1"}, "[227]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[227]"])
                }),
                CreateLogicGate(new string[] {"v2"}, "[228]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[228]"])
                }),
                CreateLogicGate(new string[] {"v0"}, "[229]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[229]"])
                }),
                CreateLogicGate(new string[] {"[227]", "[228]", "v2", "v0"}, "[13]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 0,3 }, new int[]{ 0, 1 }, outputMappings["[13]"]),
                    CreateOutputCover(new int[] { 1,3 }, new int[]{ 0, 1 }, outputMappings["[13]"])
                }),
                CreateLogicGate(new string[] {"v3"}, "[226]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[226]"])
                }),
                CreateLogicGate(new string[] {"v4"}, "[231]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0 }, new int[]{ 0 }, outputMappings["[231]"])
                }),
                CreateLogicGate(new string[] {"[229]","v5", "v4"}, "[14]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0,1 }, outputMappings["[14]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0,1 }, outputMappings["[14]"])
                }),
                CreateLogicGate(new string[] {"[13]","[226]", "[14]","[227]"}, "v7.0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["v7.0"]),
                    CreateOutputCover(new int[] { 2,3 }, new int[]{ 0,1 }, outputMappings["v7.0"])
                }),
                CreateLogicGate(new string[] {"[227]","[231]", "v6"}, "[267]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["[267]"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["[267]"])
                }),
                CreateLogicGate(new string[] {"v1","v3", "v4"}, "[269]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["[269]"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["[269]"])
                }),
                CreateLogicGate(new string[] {"[229]","[267]", "[269]","v0"}, "[195]", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 0,2 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 1,2 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 0,3 }, new int[]{ 0,1 }, outputMappings["[195]"]),
                    CreateOutputCover(new int[] { 1,3 }, new int[]{ 0,1 }, outputMappings["[195]"])
                }),
                CreateLogicGate(new string[] {"v1","v4","[195]"}, "v7.1", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] { 2 }, new int[]{ 0 }, outputMappings["v7.1"]),
                    CreateOutputCover(new int[] { 0,1 }, new int[]{ 0,1 }, outputMappings["v7.1"])
                })
            };
            Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
            for (int i = 0; i < expectedLogicGates.Count; i++)
            {
                var expectedLg = expectedLogicGates[i];
                Assert.Contains(actualModel.LogicGates, actualLogicGate => CompareLogicGatesNoAsserts(expectedLg, actualLogicGate));
            }
        }

        [Fact]
        public void MultiLineIO_Test()
        {
            string expectedModelName = "i1";
            string[] expectedInputs = new string[] {
                "V27_0", "V7_1", "V7_2", "V7_3", "V7_4", "V7_5", "V7_6", "V7_7",
                "V8_0", "V9_0", "V10_0", "V11_0", "V12_0", "V13_0", "V14_0", "V15_0", "V16_0",
                "V17_0", "V18_0", "V29_0", "V27_3", "V22_2", "V22_3", "V22_4", "V22_5"
            };
            string[] virtualInputs = new string[]
            {
                "V50", "V46", "V42", "V39", "V40", "V44", "V48", "V52",
                "V55", "V51", "V57", "V56", "V62", "V60", "V53", "V58",
                "V64", "V63", "V66", "V32_0", "V67"
            };
            string[] expectedGeneralInputs = new string[expectedInputs.Length + virtualInputs.Length];
            expectedInputs.CopyTo(expectedGeneralInputs, 0);
            virtualInputs.CopyTo(expectedGeneralInputs, expectedInputs.Length);

            Dictionary<string, int> inputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var input in expectedGeneralInputs)
            {
                inputMappings.Add(input, counter++);
            }
            int baseIdx = expectedInputs.Length - 1;
            string[] expectedOutputs = new string[] {
                "V27_0", "V27_1", "V27_2", "V27_3", "V27_4", "V28_0", "V29_0", "V30_0",
                "V31_0", "V32_0", "V33_0", "V34_0", "V35_0", "V36_0", "V37_0", "V38_0"
            };
            string[] virtualOutputs = new string[]
            {
                "V39", "V40", "V42", "V44", "V46", "V48", "V50", "V51", "V52", "V53",
                "V55", "V56", "V57", "V58", "V60", "V62", "V63", "V64", "V66", "V67",
            };
            string[] expectedGeneralOutputs = new string[expectedOutputs.Length + virtualOutputs.Length];
            expectedOutputs.CopyTo(expectedGeneralOutputs, 0);
            virtualOutputs.CopyTo(expectedGeneralOutputs, expectedOutputs.Length);

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            counter = 0;
            foreach (var output in expectedGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            var query = Utils.ReadFromFile(@"Resources/multi_line_io.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            Assert.Single(parser.Models);
            Model actualModel = parser.Models[0];
            Assert.Equal(expectedModelName, actualModel.Name);
            Assert.Equal(expectedInputs, actualModel.PrimaryInputs);
            Assert.Equal(expectedOutputs, actualModel.PrimaryOutputs);

            var generalInputsKVMap = actualModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key).ToArray();
            for (int i = 0; i < expectedGeneralInputs.Length; i++)
            {
                Assert.Equal(expectedGeneralInputs[i], sortedActualGeneralInputs[i]);
            }
            Assert.Equal(expectedGeneralInputs, sortedActualGeneralInputs);

            var generalOutputsKVMap = actualModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedGeneralOutputs, sortedActualGeneralOutputs);

            IList<LogicGate> expectedLogicGates = new List<LogicGate>()
            {
                CreateLogicGate(new List<string>() { "V7_1"}, "V39", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V39"])
                }),
                CreateLogicGate(new string[] { "V7_2"}, "V40", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V40"])
                }),
                CreateLogicGate(new string[] { "V7_3"}, "V42", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V42"])
                }),CreateLogicGate(new string[] { "V7_4"}, "V44", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V44"])
                }),CreateLogicGate(new string[] { "V7_5"}, "V46", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V46"])
                }),
                CreateLogicGate(new string[] { "V7_6"}, "V48", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V48"])
                }),CreateLogicGate(new string[] { "V7_7"}, "V50", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V50"])
                }),
                CreateLogicGate(new string[]{"V50","V46","V42","V39","V40","V44","V48"}, "V51", new List<SingleOutputCover>()
                {
                    CreateOutputCover(
                        new int[] {0,1,2,3,4,5,6}, new int[0], outputMappings["V51"])
                }),
                CreateLogicGate(new List<string>() { "V27_0"}, "V52", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V52"])
                }),
                CreateLogicGate(new List<string>() { "V52","V29_0"}, "V53", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V53"])
                }),
                CreateLogicGate(new List<string>() { "V8_0"}, "V55", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V55"])
                }),
                CreateLogicGate(new List<string>() { "V55","V29_0","V51"}, "V56", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[0], outputMappings["V56"])
                }),
                CreateLogicGate(new List<string>() { "V9_0"}, "V57", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V57"])
                }),
                CreateLogicGate(new List<string>() { "V57","V56"}, "V58", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V58"])
                }),
                CreateLogicGate(new List<string>() { "V9_0","V51","V29_0","V8_0"}, "V60", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2,3}, new int[0], outputMappings["V60"])
                }),
                CreateLogicGate(new List<string>() { "V51"}, "V62", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[] {0}, outputMappings["V62"])
                }),
                CreateLogicGate(new List<string>() { "V62","V29_0","V27_0"}, "V63", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2 }, new int[0], outputMappings["V63"])
                }),
                CreateLogicGate(new List<string>() { "V57","V51","V29_0","V8_0"}, "V64", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2,3}, new int[0], outputMappings["V64"])
                }),
                CreateLogicGate(new List<string>() { "V60","V53","V58"}, "V27_1", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[]{0,1,2}, outputMappings["V27_1"])
                }),
                CreateLogicGate(new List<string>() { "V64","V63"}, "V27_2", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[]{0,1}, outputMappings["V27_2"])
                }),
                CreateLogicGate(new List<string>() { "V27_3","V22_2"}, "V27_4", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[]{0,1}, outputMappings["V27_4"])
                }),
                CreateLogicGate(new List<string>() { "V56","V10_0"}, "V28_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[]{0,1}, outputMappings["V28_0"])
                }),
                CreateLogicGate(new List<string>() { "V22_5","V18_0"}, "V30_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V30_0"])
                }),
                CreateLogicGate(new List<string>() { "V22_5"}, "V66", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0}, new int[]{0}, outputMappings["V66"])
                }),

                CreateLogicGate(new List<string>() { "V66","V11_0"}, "V67", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V67"])
                }),
                CreateLogicGate(new List<string>() { "V22_5","V11_0"}, "V32_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V32_0"])
                }),
                CreateLogicGate(new List<string>() { "V66","V14_0","V22_3"}, "V33_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[0], outputMappings["V33_0"])
                }),
                CreateLogicGate(new List<string>() { "V66","V17_0","V22_3"}, "V34_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[0], outputMappings["V34_0"])
                }),CreateLogicGate(new List<string>() { "V66","V14_0","V22_4"}, "V35_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[0], outputMappings["V35_0"])
                }),CreateLogicGate(new List<string>() { "V66","V17_0","V22_4"}, "V36_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2}, new int[0], outputMappings["V36_0"])
                }),CreateLogicGate(new List<string>() { "V66","V16_0"}, "V37_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["V37_0"])
                }),
                CreateLogicGate(new List<string>() { "V32_0","V67"}, "V31_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1}, new int[]{0,1}, outputMappings["V31_0"])
                }),
                CreateLogicGate(new List<string>() { "V15_0","V13_0","V12_0","V14_0"}, "V38_0", new List<SingleOutputCover>()
                {
                    CreateOutputCover(new int[] {0,1,2,3}, new int[]{0,1,2,3 }, outputMappings["V38_0"])
                })
            };
            Assert.Equal(expectedLogicGates.Count, actualModel.LogicGates.Count);
            for (int i = 0; i < expectedLogicGates.Count; i++)
            {
                var expectedLg = expectedLogicGates[i];
                var actualLg = actualModel.LogicGates[i];
                Assert.True(CompareLogicGates(expectedLg, actualLg));
            }
        }

        // TODO:
        [Fact]
        public void Hierarchical_Model_Test()
        {
            int expectedModelCnt = 2;
            string expectedParentModelName = "FA";
            string[] expectedParentInputs = new string[] { "a", "b", "cin" };
            string[] expectedParentOutputs = new string[] { "s", "cout" };
            string[] expectedParentGeneralInputs = new string[] { "a", "b", "cin", "k" };
            string[] expectedParentGeneralOutputs = new string[] { "s", "cout", "k" };

            Dictionary<string, int> outputMappings = new Dictionary<string, int>();
            int counter = 0;
            foreach (var output in expectedParentGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGatesParent = new List<LogicGate>()
            {
                CreateLogicGate(new string[]{ "a", "b"}, "k", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["k"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["k"]),
                }),
                CreateLogicGate(new string[]{ "k", "cin"}, "s", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["s"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["s"]),
                }),
                CreateLogicGate(new string[]{ "a", "b", "cin"}, "cout", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["cout"]),
                    CreateOutputCover(new int[] {0,2}, new int[0], outputMappings["cout"]),
                    CreateOutputCover(new int[] {1,2}, new int[0], outputMappings["cout"]),
                })
            };

            var query = Utils.ReadFromFile(@"Resources/hierarchical_models.blif").GetAwaiter().GetResult();
            Console.WriteLine(query);
            parser.ParseQuery(query);

            Assert.NotNull(parser.Models);
            var models = parser.Models;
            Assert.Equal(expectedModelCnt, models.Count);
            DependencyTree<Model> dependencyTree = parser.ModelDependencyTree;
            Assert.NotNull(dependencyTree);
            TreeUnroller<Model> unroller = new TreeUnroller<Model>();
            var unrolledModels = unroller.Unroll(dependencyTree.DependencyNodesDict, dependencyTree.Entities, dependencyTree);

            Assert.NotNull(unrolledModels);
            Assert.Equal(expectedModelCnt, unrolledModels.Count());
            var actualParentModel = unrolledModels.First();
            Assert.Equal(expectedParentModelName, actualParentModel.Name);
            Assert.Equal(expectedParentInputs, actualParentModel.PrimaryInputs);
            Assert.Equal(expectedParentOutputs, actualParentModel.PrimaryOutputs);

            var generalInputsKVMap = actualParentModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedParentGeneralInputs, sortedActualGeneralInputs);

            var generalOutputsKVMap = actualParentModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            var sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedParentGeneralOutputs, sortedActualGeneralOutputs);

            Assert.Equal(expectedLogicGatesParent.Count, actualParentModel.LogicGates.Count);
            for (int i = 0; i < expectedLogicGatesParent.Count; i++)
            {
                var expectedLg = expectedLogicGatesParent[i];
                var actualLg = actualParentModel.LogicGates[i];
                Assert.True(CompareLogicGates(expectedLg, actualLg));
            }

            string expectedChildModelName = "Adder02";
            string[] expectedChildInputs = new string[] { "a00", "a01", "b00", "b01", "c" };
            string[] expectedChildOutputs = new string[] { "y00", "y01", "y02" };
            string[] expectedChildGeneralInputs = new string[] { "a00", "a01", "b00", "b01", "c", "tmp0", "00" };
            string[] expectedChildGeneralOutputs = new string[] { "y00", "y01", "y02", "tmp0", "00" };

            outputMappings.Clear();
            counter = 0;
            foreach (var output in expectedChildGeneralOutputs)
            {
                outputMappings.Add(output, counter++);
            }

            IList<LogicGate> expectedLogicGatesChild = new List<LogicGate>()
            {
                CreateLogicGate(new string[]{ "a00", "b00"}, "tmp0", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["tmp0"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["tmp0"]),
                }),
                CreateLogicGate(new string[]{ "tmp0", "c"}, "y00", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["y00"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["y00"]),
                }),
                CreateLogicGate(new string[]{ "a00", "b00", "c"}, "00", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["00"]),
                    CreateOutputCover(new int[] {0,2}, new int[0], outputMappings["00"]),
                    CreateOutputCover(new int[] {1,2}, new int[0], outputMappings["00"]),
                }),
                CreateLogicGate(new string[]{ "a01", "b01"}, "tmp0", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["tmp0"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["tmp0"]),
                }),
                CreateLogicGate(new string[]{ "tmp0", "00"}, "y01", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[] {1}, outputMappings["y01"]),
                    CreateOutputCover(new int[] {0,1}, new int[] {0}, outputMappings["y01"]),
                }),
                CreateLogicGate(new string[]{ "a01", "b01", "00"}, "y02", new SingleOutputCover[] {
                    CreateOutputCover(new int[] {0,1}, new int[0], outputMappings["y02"]),
                    CreateOutputCover(new int[] {0,2}, new int[0], outputMappings["y02"]),
                    CreateOutputCover(new int[] {1,2}, new int[0], outputMappings["y02"]),
                })
            };

            var actualChildModel = unrolledModels.Last();
            Assert.Equal(expectedChildModelName, actualChildModel.Name);
            Assert.Equal(expectedChildInputs, actualChildModel.PrimaryInputs);
            Assert.Equal(expectedChildOutputs, actualChildModel.PrimaryOutputs);

            generalInputsKVMap = actualChildModel.GeneralInputs.ToList();
            generalInputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            sortedActualGeneralInputs = generalInputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedChildGeneralInputs, sortedActualGeneralInputs);

            generalOutputsKVMap = actualChildModel.GeneralOutputs.ToList();
            generalOutputsKVMap.Sort((pair1, pair2) => pair1.Value.CompareTo(pair2.Value));

            sortedActualGeneralOutputs = generalOutputsKVMap.Select(x => x.Key);
            Assert.Equal(expectedChildGeneralOutputs, sortedActualGeneralOutputs);

            Assert.Equal(expectedLogicGatesChild.Count, actualChildModel.LogicGates.Count);
            for (int i = 0; i < expectedLogicGatesChild.Count; i++)
            {
                var expectedLg = expectedLogicGatesChild[i];
                var actualLg = actualChildModel.LogicGates[i];
                Assert.True(CompareLogicGates(expectedLg, actualLg));
            }
        }

        private LogicGate CreateLogicGate(IEnumerable<string> inputs, string output,
            IEnumerable<SingleOutputCover> socs)
        {
            return new LogicGate()
            {
                Socs = socs.ToList(),
                Inputs = inputs.ToList(),
                Output = output
            };
        }

        private SingleOutputCover CreateOutputCover(IEnumerable<int> inputIdxs,
            IEnumerable<int> negated, int outputIdx)
        {
            SingleOutputCover soc = new SingleOutputCover();
            ISet<int> negatedSet = negated.ToHashSet();
            soc.AddInputs(inputIdxs, negatedSet);
            soc.SetOutput(outputIdx);
            return soc;
        }

        private bool CompareLogicGates(LogicGate g1, LogicGate g2)
        {
            if (g1 == null || g2 == null) return false;
            Assert.Equal(g1.Inputs, g2.Inputs);
            Assert.Equal(g1.Output, g2.Output);
            Assert.Equal(g1.Socs.Count, g2.Socs.Count);
            for (int i = 0; i < g1.Socs.Count; i++)
            {
                var curr_LG_1 = g1.Socs[i];
                var curr_LG_2 = g2.Socs[i];
                Assert.Equal(curr_LG_1.InputIdxs, curr_LG_2.InputIdxs);
                Assert.Equal(curr_LG_1.NegatedInputsSet, curr_LG_2.NegatedInputsSet);
                Assert.Equal(curr_LG_1.OutputIdx, curr_LG_2.OutputIdx);
            }
            return true;
        }

        private bool CompareLogicGatesNoAsserts(LogicGate g1, LogicGate g2)
        {
            if (g1 == null || g2 == null) return false;
            bool equal = CompareCollections(g1.Inputs, g2.Inputs);
            equal &= g1.Output.Equals(g2.Output);
            equal &= g1.Socs.Count.Equals(g2.Socs.Count);
            if (!equal) return false;
            for (int i = 0; i < g1.Socs.Count; i++)
            {
                var curr_LG_1 = g1.Socs[i];
                var curr_LG_2 = g2.Socs[i];
                equal &= CompareCollections(curr_LG_1.InputIdxs, curr_LG_2.InputIdxs);
                equal &= CompareCollections(curr_LG_1.NegatedInputsSet, curr_LG_2.NegatedInputsSet);
                equal &= curr_LG_1.OutputIdx == curr_LG_2.OutputIdx;
            }
            return equal;
        }

        private bool CompareCollections<T>(IList<T> collection1, IList<T> collection2)
        {
            if (collection1 == null || collection2 == null) return false;
            if (collection1.Count != collection2.Count) return false;
            for (int i = 0; i < collection1.Count; i++)
            {
                if (!collection1[i].Equals(collection2[i])) return false;
            }
            return true;
        }
    }
}
