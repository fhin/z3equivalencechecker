﻿using EquivCheckerCompGen.Entities;
using System;
using Xunit;

namespace EquivCheckerTests
{
    public class DependencyTreeTests
    {
        [Fact]
        public void NoDependenciesTest()
        {
            int numEntities = 5;
            string[] entityNames = new string[numEntities];
            string[] values = new string[numEntities];
            string entityName = "Default";
            string defaultValue = "NONE";

            DependencyTree<string> tree = new DependencyTree<string>();
            for (int i = 0; i < numEntities; i++)
            {
                entityNames[i] = entityName + i;
                values[i] = defaultValue + i;
                tree.Insert(entityNames[i], null, values[i]);
            }
            Assert.Equal(numEntities, tree.DependencyNodesDict.Count);
            Assert.Equal(numEntities, tree.Entities.Count);

            DependencyNode treeNode;
            for (int i = 0; i < numEntities; i++)
            {
                Assert.True(tree.DependencyNodesDict.ContainsKey(entityNames[i]));
                treeNode = tree.DependencyNodesDict[entityNames[i]];
                Assert.Equal(entityNames[i], treeNode.Key);
                Assert.True(tree.Entities.ContainsKey(entityNames[i]));
                Assert.Equal(values[i], tree.Entities[entityNames[i]]);
                Assert.Empty(treeNode.DependentNodes);
            }
        }

        [Fact]
        public void InsertDuplicateParentNode()
        {
            string childName = "CHILD";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(childName, null, "");
            tree.Insert(childName, null, "");
            Assert.Single(tree.DependencyNodesDict);
            Assert.Single(tree.Entities);
            Assert.True(tree.Entities.ContainsKey(childName));
            Assert.True(tree.DependencyNodesDict.ContainsKey(childName));
        }

        [Fact]
        public void InsertDuplicateChildNode()
        {
            string parentName = "PARENT";
            string childName = "CHILD";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(parentName, null, "");
            tree.Insert(childName, parentName, "");
            var thrownException = Assert.ThrowsAny<Exception>(() => tree.Insert(childName, parentName, ""));
            Assert.NotNull(thrownException.InnerException);
            Assert.IsType<ArgumentException>(thrownException.InnerException);
        }

        [Fact]
        public void InsertNodeOneDependencyLevel()
        {
            string childName = "CHILD";
            string childValue = "CHILD_V";
            string parentName = "PARENT";
            string parentValue = "PARENT_V";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(parentName, null, parentValue);

            Assert.Single(tree.DependencyNodesDict);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));

            var parentNode = tree.DependencyNodesDict[parentName];
            Assert.Equal(parentName, parentNode.Key);
            Assert.Equal(parentValue, tree.Entities[parentName]);
            Assert.Empty(parentNode.DependentNodes);

            tree.Insert(childName, parentName, childValue);
            parentNode = tree.DependencyNodesDict[parentName];

            Assert.True(parentNode.DependentNodes.ContainsKey(childName));
            var childNode = parentNode.DependentNodes[childName];
            Assert.Equal(childName, childNode.Key);
            Assert.Equal(childValue, tree.Entities[childName]);
            Assert.Empty(childNode.DependentNodes);

            tree.Insert(parentName, null, parentValue);
            Assert.Single(tree.DependencyNodesDict);
            Assert.Equal(2, tree.Entities.Count);
            Assert.True(tree.Entities.ContainsKey(parentName));
            Assert.Equal(parentValue, tree.Entities[parentName]);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));
            parentNode = tree.DependencyNodesDict[parentName];
        }

        [Fact]
        public void InsertChildBeforeParent()
        {
            string childName = "CHILD";
            string childValue = "CHILD_V";
            string parentName = "PARENT";
            string parentValue = "PARENT_V";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(childName, parentName, childValue);

            Assert.Single(tree.DependencyNodesDict);
            Assert.Single(tree.Entities);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));
            Assert.True(tree.Entities.ContainsKey(childName));

            var parentNode = tree.DependencyNodesDict[parentName];
            Assert.Equal(parentName, parentNode.Key);

            Assert.Single(parentNode.DependentNodes);
            Assert.True(parentNode.DependentNodes.ContainsKey(childName));
            var childNode = parentNode.DependentNodes[childName];
            Assert.Equal(childName, childNode.Key);
            Assert.Equal(childValue, tree.Entities[childName]);
            Assert.Empty(childNode.DependentNodes);

            tree.Insert(parentName, null, parentValue);
            Assert.Single(tree.DependencyNodesDict);
            Assert.Equal(2, tree.Entities.Count);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));
            parentNode = tree.DependencyNodesDict[parentName];
            Assert.Equal(parentName, parentNode.Key);
            Assert.Equal(parentValue, tree.Entities[parentName]);
        }

        [Fact]
        public void InsertChildBeforeParentTwoLevel()
        {
            string childName = "CHILD";
            string childValue = "CHILD_V";
            string parentName = "PARENT";
            string parentValue = "PARENT_V";
            string child2Name = "CHILD_2";
            string child2Value = "CHILD_V_2";

            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(child2Name, childName, child2Value);
            tree.Insert(parentName, null, parentValue);
            tree.Insert(childName, parentName, childValue);

            Assert.Equal(1, tree.DependencyNodesDict.Count);
            Assert.Equal(3, tree.Entities.Count);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));

            DependencyNode parentNode = tree.DependencyNodesDict[parentName];
            Assert.Equal(parentName, parentNode.Key);
            Assert.Equal(parentValue, tree.Entities[parentName]);
            Assert.Equal(1, parentNode.DependentNodes.Count);

            Assert.True(parentNode.DependentNodes.ContainsKey(childName));
            DependencyNode firstChildNode = parentNode.DependentNodes[childName];
            Assert.Equal(childName, firstChildNode.Key);
            Assert.Equal(childValue, tree.Entities[childName]);
            Assert.Equal(1, firstChildNode.DependentNodes.Count);

            Assert.True(firstChildNode.DependentNodes.ContainsKey(child2Name));
            DependencyNode secChildNode = firstChildNode.DependentNodes[child2Name];
            Assert.Equal(child2Name, secChildNode.Key);
            Assert.Equal(child2Value, tree.Entities[child2Name]);
            Assert.Equal(0, secChildNode.DependentNodes.Count);
        }

        [Fact]
        public void InsertChildAfterParentsWithMultipleSameLevelParents()
        {
            int numParents = 5;
            string[] parentNames = new string[numParents];
            string[] parentValues = new string[numParents];
            string parentDefaultName = "PARENT";
            string parentDefaultValue = "PARENT_V";
            DependencyTree<string> tree = new DependencyTree<string>();

            for (int i = 0; i < numParents; i++)
            {
                parentNames[i] = parentDefaultName + i;
                parentValues[i] = parentDefaultValue + i;
                tree.Insert(parentNames[i], null, parentValues[i]);
            }
            string childName = "CHILD";
            string childValue = "CHILD_V";
            tree.Insert(childName, childValue, parentNames);
            Assert.Equal(numParents, tree.DependencyNodesDict.Count);
            Assert.Equal(numParents + 1, tree.Entities.Count);
            DependencyNode parentNode;
            DependencyNode childNode;
            foreach (var parentName in parentNames)
            {
                Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));
                Assert.True(tree.Entities.ContainsKey(parentName));
                parentNode = tree.DependencyNodesDict[parentName];
                Assert.Single(parentNode.DependentNodes);
                childNode = parentNode.DependentNodes[childName];
                Assert.NotNull(childNode);
                Assert.Equal(childName, childNode.Key);
                Assert.Equal(childValue, tree.Entities[childName]);
            }
        }

        [Fact]
        public void InsertChildBeforeParentWithMultipleSameLevelParents()
        {
            int numParents = 5;
            string[] parentNames = new string[numParents];
            string[] parentValues = new string[numParents];
            string parentDefaultName = "PARENT";
            string parentDefaultValue = "PARENT_V";
            DependencyTree<string> tree = new DependencyTree<string>();

            for (int i = 0; i < numParents; i++)
            {
                parentNames[i] = parentDefaultName + i;
                parentValues[i] = parentDefaultValue + i;
            }
            string childName = "CHILD";
            string childValue = "CHILD_V";
            tree.Insert(childName, childValue, parentNames);
            Assert.Equal(numParents, tree.DependencyNodesDict.Count);
            Assert.Single(tree.Entities);
            DependencyNode parentNode;
            DependencyNode childNode;
            foreach (var parentName in parentNames)
            {
                Assert.True(tree.DependencyNodesDict.ContainsKey(parentName));
                parentNode = tree.DependencyNodesDict[parentName];
                Assert.Single(parentNode.DependentNodes);
                childNode = parentNode.DependentNodes[childName];
                Assert.NotNull(childNode);
                Assert.Equal(childName, childNode.Key);
                Assert.Equal(childValue, tree.Entities[childName]);
            }
            string currParentName;
            for (int i = 0; i < numParents; i++)
            {
                currParentName = parentNames[i];
                tree.Insert(currParentName, null, parentValues[i]);
                Assert.True(tree.Entities.ContainsKey(currParentName));
                var parentNodeEntity = tree.Entities[currParentName];
                Assert.Equal(parentValues[i], parentNodeEntity);
            }
            Assert.Equal(numParents + 1, tree.Entities.Count);
            Assert.Equal(numParents, tree.DependencyNodesDict.Count);
        }

        [Fact]
        public void InsertChildAfterParentsSameLevelsNoChildren()
        {
            string topLevelParentName_1 = "T_PARENT_1";
            string topLevelParentName_2 = "T_PARENT_2";
            string childName = "CHILD";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(topLevelParentName_1, null, "");
            tree.Insert(topLevelParentName_2, null, "");
            tree.Insert(childName, "", new string[] { topLevelParentName_1, topLevelParentName_2 });

            int expectedNumParents = 2;
            int expectedNumEntities = 3;
            Assert.Equal(expectedNumParents, tree.DependencyNodesDict.Count);
            Assert.Equal(expectedNumEntities, tree.Entities.Count);
            Assert.True(tree.DependencyNodesDict.ContainsKey(topLevelParentName_1));
            Assert.True(tree.DependencyNodesDict.ContainsKey(topLevelParentName_2));
            var topLevelP = tree.DependencyNodesDict[topLevelParentName_1];
            Assert.Single(topLevelP.DependentNodes);
            Assert.True(topLevelP.DependentNodes.ContainsKey(childName));

            topLevelP = tree.DependencyNodesDict[topLevelParentName_2];
            Assert.Single(topLevelP.DependentNodes);
            Assert.True(topLevelP.DependentNodes.ContainsKey(childName));
        }

        [Fact]
        public void InsertChildToExistingDependency()
        {
            string parentDefaultName = "PARENT";
            string parentDefaultValue = "PARENT_V";
            string firstChildName = "F_CHILD";
            string firstChildValue = "F_CHILD_V";
            string newChildName = "NEW_CHILD";
            string newChildValue = "NEW_CHILD_V";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(parentDefaultName, null, parentDefaultValue);
            tree.Insert(firstChildName, parentDefaultName, firstChildValue);
            tree.Insert(newChildName, parentDefaultName, newChildValue);

            int expectedChildCnt = 2;
            int expectedNumEntities = 3;
            Assert.Single(tree.DependencyNodesDict);
            Assert.Equal(expectedNumEntities, tree.Entities.Count);
            Assert.True(tree.DependencyNodesDict.ContainsKey(parentDefaultName));

            var parentNode = tree.DependencyNodesDict[parentDefaultName];
            Assert.Equal(expectedChildCnt, parentNode.DependentNodes.Count);
            Assert.True(parentNode.DependentNodes.ContainsKey(firstChildName));
            Assert.True(parentNode.DependentNodes.ContainsKey(newChildName));
        }

        [Fact]
        public void InsertChildWithParentsDifferentLevelsWithChildren()
        {
            string topLevelParentName_1 = "T_PARENT_1";
            string topLevelParentName_2 = "T_PARENT_2";
            string b_LevelParentName = "B_PARENT";
            string childName = "CHILD";
            DependencyTree<string> tree = new DependencyTree<string>();
            tree.Insert(topLevelParentName_1, null, "");
            tree.Insert(topLevelParentName_2, null, "");
            tree.Insert(b_LevelParentName, topLevelParentName_2, "");
            tree.Insert(childName, "", new string[] { topLevelParentName_1, b_LevelParentName });

            int expectedNumParents = 2;
            int expectedNumEntities = 4;
            Assert.Equal(expectedNumParents, tree.DependencyNodesDict.Count);
            Assert.Equal(expectedNumEntities, tree.Entities.Count);
            Assert.True(tree.DependencyNodesDict.ContainsKey(topLevelParentName_1));
            Assert.True(tree.DependencyNodesDict.ContainsKey(topLevelParentName_2));

            var topLevelP = tree.DependencyNodesDict[topLevelParentName_1];
            Assert.Single(topLevelP.DependentNodes);
            Assert.True(topLevelP.DependentNodes.ContainsKey(childName));

            topLevelP = tree.DependencyNodesDict[topLevelParentName_2];
            Assert.Single(topLevelP.DependentNodes);
            Assert.True(topLevelP.DependentNodes.ContainsKey(b_LevelParentName));

            var b_LevelP = topLevelP.DependentNodes[b_LevelParentName];
            Assert.Single(b_LevelP.DependentNodes);
            Assert.True(b_LevelP.DependentNodes.ContainsKey(childName));

        }
    }
}
