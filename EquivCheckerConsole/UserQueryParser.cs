﻿using EquivCheckerCompGen.Entities;
using System;
using System.Collections.Generic;
using System.IO;

namespace EquivCheckerConsole
{
    public class UserQueryParser
    {
        public IList<Model> Models { get; private set; }
        public bool IsValidQuery { get; private set; }
        public IList<QueryError> Errors { get; private set; }

        public DependencyTree<Model> ModelDependencyTree { get; private set; }

        private bool _rethrowExceptionAfterLogging;

        public UserQueryParser()
        {
            IsValidQuery = false;
            Errors = new List<QueryError>();
            _rethrowExceptionAfterLogging = false;
        }

        public void RethrowExceptionsAfterLogging()
        {
            _rethrowExceptionAfterLogging = true;
        }

        public void ParseQuery(string queryString)
        {
            if (string.IsNullOrWhiteSpace(queryString)) throw new ArgumentException("");
            Exception backupException = null;
            try
            {
                using (var stream = new MemoryStream())
                {
                    using (var writer = new StreamWriter(stream))
                    {
                        writer.Write(queryString);
                        writer.Flush();
                        stream.Position = 0;

                        Scanner s = new Scanner(stream);

                        using (var parserOutputRedirect = new ParserOutputRedirect())
                        {
                            Parser p = new Parser(s);
                            p.errors.errorStream = parserOutputRedirect;
                            p.Parse();
                            Errors = parserOutputRedirect.QueryErrors;
                            Models = p._models;
                            ModelDependencyTree = p._modelDependencyTree;
                        }

                        if (Errors.Count == 0)
                        {
                            IsValidQuery = true;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error parsing query for reason " + e.Message);
                Console.WriteLine("Stacktrace: " + e.StackTrace);
                IsValidQuery = false;
                if (_rethrowExceptionAfterLogging)
                {
                    backupException = e;
                }
            }
            finally
            {
                if (backupException != null && _rethrowExceptionAfterLogging)
                {
                    throw backupException;
                }
            }
        }
    }
}

