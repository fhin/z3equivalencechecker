﻿using EquivCheckerCompGen;
using EquivCheckerCompGen.Entities;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EquivCheckerConsole
{
    public class Utils
    {
        private static readonly Regex singleCoverPatternRegex = new Regex("^((0|1|-)+)|\\.(model|input|output|subckt|names|end)",
           RegexOptions.Compiled | RegexOptions.Singleline | RegexOptions.IgnoreCase);
        private static readonly Regex replaceMultipleWhitespacesRegex = new Regex("[ ]{2,}"
            , RegexOptions.Compiled | RegexOptions.Singleline);

        public static async Task<string> ReadFromFile(string path)
        {
            if (path == null || string.IsNullOrWhiteSpace(path) || !File.Exists(path))
            {
                throw new ArgumentException("Cannot open file for given path: " + path);
            }
            var fileLinesTask = File.ReadAllLinesAsync(path);
            try
            {
                string[] filesLines = await fileLinesTask;
                ConcurrentQueue<string> fileLinesToParse = new ConcurrentQueue<string>();
                StringBuilder builder = new StringBuilder();
                StringBuilder tmpBuilder = new StringBuilder();
                bool appendedLines = false;
                bool parsedAllLines = false;
                var lockObj = new object();

                Task<string> fileLineParsingTask = Task.Run(() =>
                {
                    StringBuilder builder = new StringBuilder();
                    string currLine;
                    bool continueParsing = true;

                    while (continueParsing)
                    {
                        if (fileLinesToParse.TryDequeue(out currLine))
                        {
                            builder.Append(ParseLine(currLine));
                        }
                        if (!fileLinesToParse.IsEmpty)
                        {
                            continue;
                        }
                        else
                        {
                            lock (lockObj)
                            {
                                continueParsing = !parsedAllLines;
                            }
                        }
                    }
                    return builder.ToString();
                });

                string currLine;
                for (int i = 0; i < filesLines.Length; i++)
                {
                    currLine = filesLines[i];
                    if (currLine.Contains('\\'))
                    {
                        currLine = currLine.Replace("\\", " ");
                        tmpBuilder.Append(currLine);
                        appendedLines = true;
                    }
                    else
                    {
                        if (appendedLines)
                        {
                            tmpBuilder.Append(currLine);
                            fileLinesToParse.Enqueue(tmpBuilder.ToString());
                            tmpBuilder.Clear();
                            appendedLines = false;
                        }
                        else
                        {
                            fileLinesToParse.Enqueue(currLine);
                        }
                    }
                }
                lock (lockObj)
                {
                    parsedAllLines = true;
                }
                return await fileLineParsingTask;
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error parsing defined model defined in given file at " + path
                    + "\n Reason [for further information see inner exception]: " + e.Message, e);
            }
        }

        private static string ParseLine(string line)
        {
            if (line == null || string.IsNullOrWhiteSpace(line)) return line;

            line = replaceMultipleWhitespacesRegex.Replace(line, " ");
            if (line.Contains(Constants.COMMENT_START_SYMB))
            {
                return line + "\n";
            }
            Match m = singleCoverPatternRegex.Match(line);
            string prefix;
            if (m.Success)
            {
                switch (m.Value)
                {
                    case ".model":
                        prefix = Constants.MODEL_START.ToString();
                        break;
                    case ".end":
                        prefix = Constants.MODEL_END.ToString();
                        break;
                    case ".input":
                        prefix = Constants.INPUT_START.ToString();
                        break;
                    case ".output":
                        prefix = Constants.OUTPUT_START.ToString();
                        break;
                    case ".names":
                        prefix = Constants.NAMES_START.ToString();
                        break;
                    case ".subckt":
                        prefix = Constants.MODEL_REF_START.ToString();
                        break;
                    default:
                        prefix = Constants.SINGLE_COVER_START.ToString();
                        string tmpString = "";
                        int cutIdx = 0;
                        for (int i = 0; i < line.Length; i++)
                        {
                            if (line[i].Equals('1') || line[i].Equals('0') || line[i].Equals('-'))
                            {
                                tmpString += line[i] + Constants.MODEL_START.ToString();
                            }
                            else
                            {
                                cutIdx = i;
                                break;
                            }
                        }
                        if (cutIdx > 0)
                        {
                            tmpString = tmpString.Substring(0, tmpString.Length - 1);
                        }
                        line = tmpString + line.Substring(cutIdx);
                        break;
                }
                line = prefix + line;
                line = line.Replace(' ', Constants.DELIMITER);
            }
            return line;
        }

        public static async Task<IEnumerable<Model>> CreateModelsFromFile(string path, bool printGeneratedQuery)
        {
            Task<string> queryFromFile = ReadFromFile(path);
            var generatedQuery = await queryFromFile;
            if (printGeneratedQuery)
            {
                Console.WriteLine(generatedQuery);
            }
            return CreateModelsFromQuery(generatedQuery);
        }

        public static IEnumerable<Model> CreateModelsFromQuery(string queryText)
        {
            UserQueryParser parser = new UserQueryParser();
            try
            {
                parser.ParseQuery(queryText);
            }
            catch (Exception e)
            {
                Console.WriteLine("Error reading from given file for reason: " + e.Message);
            }
            string errMsgFormat = "-- Line: {0} | Column: {1} | Msg: {2}";

            if (!parser.IsValidQuery)
            {
                Console.WriteLine("\n=================");
                Console.WriteLine("Found Errors: ");
                foreach (var err in parser.Errors)
                {
                    Console.WriteLine(string.Format(errMsgFormat, new object[] { err.Line, err.Column, err.ErrorMsg }));
                }
                Console.WriteLine("=================\n");
                throw new ArgumentException("Could not create model/s for given query !");
            }
            return parser.Models;
        }

        public static string Repeat(string stringToRep, int numReps)
        {
            return RepeatAsMutuable(stringToRep, numReps).ToString();
        }

        public static StringBuilder RepeatAsMutuable(string stringToRep, int numReps)
        {
            return new StringBuilder(numReps).Insert(0, stringToRep, numReps);
        }

        public static string MessageWithHeader(object content, string header)
        {
            string message = "";
            if (string.IsNullOrWhiteSpace(header)) return message;
            message += "==========================================\n";
            message += header.ToString() + "\n";
            message += "==========================================\n";
            if (content != null)
            {
                message += content.ToString() + "\n";
                message += "==========================================\n";
            }
            return message;
        }
    }
}
