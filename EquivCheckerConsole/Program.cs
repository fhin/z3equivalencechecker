﻿using EquivCheckerCompGen;
using Microsoft.Z3;
using System;
using System.Diagnostics;
using System.IO;

namespace EquivCheckerConsole
{
    class Program
    {
        private static readonly char argStartChar = '-';
        private static string circuit_1_path = "";
        private static string circuit_2_path = "";
        private static bool printMiter = false;
        private static bool printModels = false;
        private static bool printAllSATAssignments = false;
        private static bool printSimplifiedExpr = false;
        private static bool useLogFile = false;
        private static string basePath = "";
        private static string logFilePath = "";
        private static readonly string availableParams = "-m1 <circuit_file_1> -m2 <circuit_file_2> [-pMiter] [-pAllAssigns] [-pModels] [-circuitBasePath <basePath>] [-useLogFile] [-logFilePath <logFilePath>]\n";
        static void Main(string[] args)
        {
            try
            {
                var oldOut = new StreamWriter(Console.OpenStandardOutput())
                {
                    AutoFlush = true
                };
                basePath = AppDomain.CurrentDomain.BaseDirectory;

                bool parseArgsErrors = false;
                string parseArgsOutput;
                using (var stringWriter = new StringWriter())
                {
                    Console.SetOut(stringWriter);
                    Console.SetError(stringWriter);
                    try
                    {
                        ParseArgs(args);
                    }
                    catch (Exception e)
                    {
                        parseArgsErrors = true;
                        stringWriter.WriteLine(e.Message);
                    }
                    finally
                    {
                        parseArgsOutput = stringWriter.ToString();
                        Console.SetOut(oldOut);
                        Console.SetError(oldOut);
                    }
                }
                if (useLogFile)
                {
                    string logFileExtenstion = ".txt";
                    string logfileName = "CMP_M1_" + ExtractFileName(circuit_1_path) + "_M2_" + ExtractFileName(circuit_2_path) +
                        logFileExtenstion;
                    string logfileBasePath;
                    if (string.IsNullOrWhiteSpace(Program.logFilePath))
                    {
                        logfileBasePath = AppDomain.CurrentDomain.BaseDirectory;
                    }
                    else
                    {
                        logfileBasePath = Program.logFilePath;
                    }
                    string logFilePath = Path.Combine(new string[] { logfileBasePath, logfileName });
                    try
                    {
                        FileStream logFileStream = new FileStream(logFilePath, FileMode.OpenOrCreate, FileAccess.Write);
                        StreamWriter logFileWriter = new StreamWriter(logFileStream);
                        logFileWriter.AutoFlush = true;
                        using (logFileStream)
                        {
                            using (logFileWriter)
                            {
                                Console.SetOut(logFileWriter);
                                Console.SetError(logFileWriter);
                                Console.Write(parseArgsOutput);
                                Console.WriteLine("Using log file @ " + logFilePath);
                                if (!parseArgsErrors)
                                {
                                    RunEquivalenceCheck(basePath, circuit_1_path, circuit_2_path);
                                }
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        throw new Exception("Could not redirect output to log file " + logFilePath + "\n" +
                            "Reason: " + e.Message);
                    }
                    finally
                    {
                        Console.SetOut(oldOut);
                        Console.SetError(oldOut);
                    }
                }
                else
                {
                    Console.WriteLine(parseArgsOutput);
                    RunEquivalenceCheck(basePath, circuit_1_path, circuit_2_path);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error running equivalence check, reason: " + e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: " + e.InnerException.Message);
                }
            }
        }

        private static string ExtractFileName(string fileName)
        {
            if (fileName.Contains('.'))
            {
                int dotIndex = fileName.IndexOf('.');
                return fileName.Substring(0, dotIndex);
            }
            return fileName;
        }

        private static void RunEquivalenceCheck(string basePath, string circuit_1_path, string circuit_2_path)
        {
            Stopwatch stopwatch = new Stopwatch();
            try
            {
                circuit_1_path = Path.Combine(basePath, circuit_1_path);
                circuit_2_path = Path.Combine(basePath, circuit_2_path);
                stopwatch.Start();

                (BoolExpr simplifiedExpr, Status status) = EquivChecker.CheckSAT(circuit_1_path, circuit_2_path, printMiter, printAllSATAssignments,
                    printModels).GetAwaiter().GetResult();
                if (printSimplifiedExpr)
                {
                    Console.WriteLine(Utils.MessageWithHeader(simplifiedExpr, "Simplified Miter expression: "));
                }
                if (status == Status.UNSATISFIABLE)
                {
                    Console.WriteLine("Given circuits are equivalent !\n");
                }
                else if (status == Status.SATISFIABLE)
                {
                    Console.WriteLine("Given circuits are not equivalent !\n");
                }
                else
                {
                    Console.WriteLine("Cannot reason if circuits are equivalent or not !\n");
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("Error checking equivalence, reason: " + e.Message);
                if (e.InnerException != null)
                {
                    Console.WriteLine("Inner exception: " + e.InnerException.Message);
                }
            }
            finally
            {
                stopwatch.Stop();
                Console.WriteLine("Summary");
                EquivChecker.PrintZ3Info();
                Console.WriteLine("Elapsed time [in ms]: " + stopwatch.ElapsedMilliseconds);
            }
        }

        private static void ParseArgs(string[] args)
        {
            //if (args == null || args.Length == 0) return;
            Console.WriteLine(Utils.MessageWithHeader(null, "Provided launch arguments"));
            string providedArg;
            for (int i = 0; i < args.Length; i++)
            {
                providedArg = args[i];
                if (!providedArg.StartsWith(argStartChar)) continue;
                providedArg = providedArg.Remove(0, 1);
                switch (providedArg)
                {
                    case "m1":
                        if (i + 1 < args.Length)
                        {
                            circuit_1_path = args[i + 1];
                            Console.WriteLine("Using 1st circuit file: " + circuit_1_path);
                        }
                        else
                        {
                            throw new ArgumentException("Path for first circuit was not given !");
                        }
                        break;
                    case "m2":
                        if (i + 1 < args.Length)
                        {
                            circuit_2_path = args[i + 1];
                            Console.WriteLine("Using 2nd circuit file: " + circuit_2_path);
                        }
                        else
                        {
                            throw new ArgumentException("Path for second circuit was not given !");
                        }
                        break;
                    case "pMiter":
                        printMiter = true;
                        break;
                    case "pAllAssigns":
                        printAllSATAssignments = true;
                        break;
                    case "pModels":
                        printModels = true;
                        break;
                    case "basePath":
                        if (i + 1 < args.Length)
                        {
                            basePath = args[i + 1];
                            Console.WriteLine("Using base path for circuit files: " + basePath);
                        }
                        else
                        {
                            throw new ArgumentException("No base path for circuit files given !");
                        }
                        break;
                    case "pSimplified":
                        printSimplifiedExpr = true;
                        break;
                    case "useLogFile":
                        useLogFile = true;
                        break;
                    case "logFilePath":
                        if (i + 1 < args.Length)
                        {
                            logFilePath = args[i + 1];
                            Console.WriteLine("Using base path for log file: " + logFilePath);
                        }
                        else
                        {
                            throw new ArgumentException("No log file path given !");
                        }
                        break;
                }
            }
            if (string.IsNullOrWhiteSpace(circuit_1_path) || string.IsNullOrWhiteSpace(circuit_2_path))
            {
                throw new ArgumentException(availableParams + "To check for equivalence two circuit files must be provided !");
            }

            Console.WriteLine("Printing Miter expression: " + printMiter);
            Console.WriteLine("Printing all satisfying assignments: " + printAllSATAssignments);
            Console.WriteLine("Printing all found models: " + printModels);
            Console.WriteLine("==========================================\n");
        }
    }
}
