﻿using EquivCheckerCompGen.Entities;
using EquivCheckerConsole;
using Microsoft.Z3;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EquivCheckerCompGen
{
    public class EquivChecker
    {
        private IList<string> _primaryModelOutputs;
        private readonly Context context;
        private readonly object _lockObj;

        public string Prefix { get; private set; }
        public IDictionary<string, BoolExpr> LiteralExprLookupDict { get; private set; }

        public EquivChecker(Context context, object lockObj)
        {
            LiteralExprLookupDict = new Dictionary<string, BoolExpr>();
            this.context = context;
            _lockObj = lockObj;
        }

        private void FillDicts(Entities.Model model)
        {
            string currElemKey;
            lock (_lockObj)
            {
                currElemKey = Prefix;
                for (int i = 0; i < model.PrimaryInputs.Length; i++)
                {
                    currElemKey += model.PrimaryInputs[i];
                    LiteralExprLookupDict.TryAdd(currElemKey, context.MkBoolConst(currElemKey));
                    currElemKey = Prefix;
                }
                for (int i = 0; i < model.PrimaryOutputs.Length; i++)
                {
                    currElemKey += model.PrimaryOutputs[i];
                    LiteralExprLookupDict.TryAdd(currElemKey, context.MkBoolConst(currElemKey));
                    currElemKey = Prefix;
                }
            }
        }

        public static void PrintZ3Info()
        {
            Console.WriteLine("Z3 - Infos:");
            Console.WriteLine("===============================");
            Microsoft.Z3.Global.ToggleWarningMessages(true);
            Console.Write("Z3 Major Version: ");
            Console.WriteLine(Microsoft.Z3.Version.Major.ToString());
            Console.Write("Z3 Full Version: ");
            Console.WriteLine(Microsoft.Z3.Version.ToString());
            Console.Write("Z3 Full Version String: ");
            Console.WriteLine(Microsoft.Z3.Version.FullVersion);
            Console.WriteLine("===============================");
        }

        public void CreateExpressionsForModel(Entities.Model model)
        {
            if (model == null || context == null) throw new ArgumentException();
            Prefix = model.Name;
            FillDicts(model);
            foreach (var logicGate in model.LogicGates)
            {
                LiteralExprLookupDict[Prefix + logicGate.Output] = CreateExpressionForLogicGate(logicGate);
            }
            _primaryModelOutputs = model.PrimaryOutputs;
        }

        public BoolExpr CreateExpressionForLogicGate(LogicGate lg)
        {
            if (lg == null) throw new ArgumentException(ErrorMsgBuilder.EMPTY_LOGIC_GATE);
            IList<BoolExpr> singleOutputCoverExprs = new List<BoolExpr>();
            int currNegatedIdx;
            int currNegatedInputIdx;
            int numNegatedInputs;

            foreach (var singleOutputCover in lg.Socs)
            {
                currNegatedIdx = 0;
                var expressions = new List<BoolExpr>();
                string currInput = "";
                currNegatedInputIdx = singleOutputCover.NegatedInputsSet.Any()
                    ? singleOutputCover.NegatedInputsSet[currNegatedIdx] : -1;
                numNegatedInputs = singleOutputCover.NegatedInputsSet.Count;

                for (int i = 0; i < singleOutputCover.InputIdxs.Count; i++)
                {
                    currInput = Prefix + lg.Inputs[singleOutputCover.InputIdxs[i]];
                    var currLiteralExpr = LiteralExprLookupDict[currInput];
                    if (i == currNegatedInputIdx)
                    {
                        lock (_lockObj)
                        {
                            currLiteralExpr = context.MkNot(currLiteralExpr);
                        }
                        currNegatedIdx = currNegatedIdx + 1 < numNegatedInputs ? currNegatedIdx + 1 : -1;
                        if (currNegatedIdx != -1)
                        {
                            currNegatedInputIdx = singleOutputCover.NegatedInputsSet[currNegatedIdx];
                        }
                    }
                    expressions.Add(currLiteralExpr);
                }
                BoolExpr singleOutputCoverExpr;
                if (expressions.Count > 1)
                {
                    lock (_lockObj)
                    {
                        singleOutputCoverExpr = context.MkAnd(expressions);
                    }
                }
                else
                {
                    singleOutputCoverExpr = expressions[0];
                }
                if (singleOutputCover.IsOutputOff)
                {
                    lock (_lockObj)
                    {
                        singleOutputCoverExpr = context.MkNot(singleOutputCoverExpr);
                    }
                }
                singleOutputCoverExprs.Add(singleOutputCoverExpr);
            }
            if (singleOutputCoverExprs.Count > 1)
            {
                lock (_lockObj)
                {
                    return context.MkOr(singleOutputCoverExprs);
                }
            }
            else
            {
                return singleOutputCoverExprs.First();
            }
        }

        public static async Task<BoolExpr> CreateMiter(Context context, Entities.Model model_1, Entities.Model model_2)
        {
            if (model_1 == null || model_2 == null) return null;
            var lockObj = new object();
            if (model_1.Name != null && model_2.Name != null && model_1.Name.Equals(model_2.Name))
            {
                model_1.Name = "m1_" + model_1.Name;
                model_2.Name = "m2_" + model_2.Name;
            }
            IEnumerable<string> sharedPrimaryInputs = MergePrimaryInputs(model_1, model_2);
            Task<EquivChecker> genTask_1 = new Task<EquivChecker>(() =>
            {
                EquivChecker checker = new EquivChecker(context, lockObj);
                if (string.IsNullOrWhiteSpace(model_1.Name))
                {
                    model_1.Name = "m1_";
                }
                checker.CreateExpressionsForModel(model_1);
                return checker;
            });
            Task<EquivChecker> genTask_2 = new Task<EquivChecker>(() =>
            {
                EquivChecker checker = new EquivChecker(context, lockObj);
                if (string.IsNullOrWhiteSpace(model_2.Name))
                {
                    model_2.Name = "m2_";
                }
                checker.CreateExpressionsForModel(model_2);
                return checker;
            });
            try
            {
                genTask_1.Start();
                genTask_2.Start();
                EquivChecker checker_1 = await genTask_1;
                EquivChecker checker_2 = await genTask_2;
                if (checker_1 == null || checker_2 == null) throw new ArgumentException("");
                if (checker_1._primaryModelOutputs == null || checker_2._primaryModelOutputs == null
                    || checker_1._primaryModelOutputs.Count != checker_2._primaryModelOutputs.Count)
                {
                    throw new ArgumentException(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.PRIMARY_OUTPUT_CNT_MISSMATCH, new object[]
                    {
                        checker_1._primaryModelOutputs.Count, checker_2._primaryModelOutputs.Count
                    }));
                }

                var miterExpressions = new List<BoolExpr>();
                BoolExpr output_model_1_expr;
                BoolExpr output_model_2_expr;
                string outputName_Model1;
                string outputName_Model2;

                Task<BoolExpr> primaryInputEquivEquation = checker_1.CreatePrimaryInputEquivEquation(checker_1.Prefix,
                    checker_2.Prefix, sharedPrimaryInputs, checker_1.LiteralExprLookupDict, checker_2.LiteralExprLookupDict);
                for (int i = 0; i < checker_1._primaryModelOutputs.Count; i++)
                {
                    outputName_Model1 = checker_1.Prefix + checker_1._primaryModelOutputs[i];
                    outputName_Model2 = checker_2.Prefix + checker_2._primaryModelOutputs[i];
                    output_model_1_expr = checker_1.LiteralExprLookupDict[outputName_Model1];
                    output_model_2_expr = checker_2.LiteralExprLookupDict[outputName_Model2];
                    miterExpressions.Add(context.MkXor(output_model_1_expr, output_model_2_expr));
                }
                BoolExpr miterExpr = miterExpressions.Count > 1 ? context.MkOr(miterExpressions) : miterExpressions[0];
                BoolExpr pInputExpr = await primaryInputEquivEquation;
                return context.MkAnd(pInputExpr, miterExpr);

            }
            catch (Exception e)
            {
                throw new Exception("Error creating miter structure for reason: " + e.Message, e);
            }
        }

        public static (BoolExpr, Status) CheckSAT(Context context, BoolExpr miterExpression, bool showAllSatisfyingAssignments)
        {
            if (miterExpression == null) throw new ArgumentException("TODO");
            try
            {
                Solver solver = context.MkSolver();
                Tactic simplifyTactic = context.MkTactic("simplify");
                Goal g = context.MkGoal();
                g.Assert(miterExpression);
                ApplyResult simplifyResult = simplifyTactic.Apply(g);
                if (simplifyResult.NumSubgoals < 1)
                {
                    throw new ArgumentException("Could not simplify created miter expression !");
                }
                BoolExpr simplifiedExpr = simplifyResult.Subgoals.First().AsBoolExpr();
                solver.Assert(simplifiedExpr);
                Status status = solver.Check();
                if (showAllSatisfyingAssignments)
                {
                    StringBuilder satAssignmentBuilder = new StringBuilder();
                    string assignmentFormat = "Name: {0} | Value: {1}\n";
                    int numDifferentAssignments = 0;
                    string delimiter = "|-----------------------------------|\n";
                    if (status != Status.SATISFIABLE) return (simplifiedExpr, status);
                    while (status == Status.SATISFIABLE)
                    {
                        satAssignmentBuilder.Append("\n" + delimiter);
                        satAssignmentBuilder.Append("Assigment #" + numDifferentAssignments++ + ":\n");
                        satAssignmentBuilder.Append(delimiter);
                        using (Microsoft.Z3.Model model = solver.Model)
                        {
                            string constName;
                            bool constValue;
                            foreach (var modelConst in model.Consts)
                            {
                                constName = modelConst.Key.Name.ToString();
                                if (modelConst.Value.IsBool)
                                {
                                    constValue = modelConst.Value.IsTrue ? true : false;
                                    satAssignmentBuilder.Append(string.Format(assignmentFormat, new object[] { constName, constValue }));
                                    solver.Add(context.MkNot(context.MkEq(context.MkBoolConst(modelConst.Key.Name), context.MkBool(constValue))));
                                }
                            }
                            status = solver.Check();
                        }
                    }
                    status = Status.SATISFIABLE;
                    string msgTitle = " FOUND SAT ASSIGNMENTS \n\n";
                    string header = msgTitle + "Found " + numDifferentAssignments + " different assignments: \n" +
                        "Format for each assignment name entry is <modelName>_<variable_name>";
                    Console.WriteLine(Utils.MessageWithHeader(satAssignmentBuilder.ToString(), header));
                }
                return (simplifiedExpr, status);
            }
            catch (Exception e)
            {
                throw new ArgumentException("Error checking satisfiabiliy of given miter expression, for reason: " + e.Message, e);
            }
        }

        public static async Task<(BoolExpr, Status)> CheckSAT(string circuitFilePath_1, string circuitFilePath_2, bool printMiterExpr,
            bool printAllSATAssignments, bool printModels)
        {
            try
            {
                Task<IEnumerable<Entities.Model>> models_First = Utils.CreateModelsFromFile(circuitFilePath_1, printModels);
                Task<IEnumerable<Entities.Model>> models_Sec = Utils.CreateModelsFromFile(circuitFilePath_2, printModels);

                Console.WriteLine("If multiple models are defined in a circuit file, the first defined one will be used ! \n");
                var model_1 = (await models_First).First();
                var model_2 = (await models_Sec).First();
                using (var context = new Microsoft.Z3.Context())
                {
                    BoolExpr finalExpr = await CreateMiter(context, model_1, model_2);
                    if (printMiterExpr)
                    {
                        Console.WriteLine(Utils.MessageWithHeader(finalExpr.ToString(), "Miter expression: "));
                    }
                    return CheckSAT(context, finalExpr, printAllSATAssignments);
                }
            }
            catch (Exception e)
            {
                throw new Exception("Could not check equivalence for given circuits, for further information see the inner exception", e);
            }
        }

        private Task<BoolExpr> CreatePrimaryInputEquivEquation(string model1_prefix, string model2_prefix,
            IEnumerable<string> sharedPrimaryInputs, IDictionary<string, BoolExpr> literalExprDict_model1,
            IDictionary<string, BoolExpr> literalExprDict_model2)
        {
            string pInput_model1;
            string pInput_model2;
            BoolExpr sharedPInputExpr_model1;
            BoolExpr sharedPInputExpr_model2;
            IList<BoolExpr> equivalenceExpr = new List<BoolExpr>();
            foreach (var sharedPInput in sharedPrimaryInputs)
            {
                pInput_model1 = model1_prefix + sharedPInput;
                pInput_model2 = model2_prefix + sharedPInput;
                sharedPInputExpr_model1 = literalExprDict_model1[pInput_model1];
                sharedPInputExpr_model2 = literalExprDict_model2[pInput_model2];
                lock (_lockObj)
                {
                    //   equivalenceExpr.Add(context.MkEq(sharedPInputExpr_model1, sharedPInputExpr_model2));
                    equivalenceExpr.Add(context.MkAnd(sharedPInputExpr_model1, sharedPInputExpr_model2));
                }
            }
            BoolExpr finalEquivalenceExpr = null;
            lock (_lockObj)
            {
                finalEquivalenceExpr = context.MkAnd(equivalenceExpr.AsEnumerable());
            }
            return Task.FromResult(finalEquivalenceExpr);
        }

        private static IEnumerable<string> MergePrimaryInputs(Entities.Model model_1, Entities.Model model_2)
        {
            if (model_1 == null || model_2 == null) return new string[0];
            IEnumerable<string> sharedPrimaryInputs = Enumerable.Intersect(model_1.PrimaryInputs, model_2.PrimaryInputs);
            return sharedPrimaryInputs;
        }
    }
}
