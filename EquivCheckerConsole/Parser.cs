using EquivCheckerCompGen;
using EquivCheckerCompGen.Entities;
using System;
using System.Collections.Generic;
using System.Linq;



public class Parser
{
    public const int _EOF = 0;
    public const int _delimiter = 1;
    public const int _o_bracket = 2;
    public const int _c_bracket = 3;
    public const int _prefix = 4;
    public const int _single_cover_start = 5;
    public const int _input_start = 6;
    public const int _output_start = 7;
    public const int _names_start = 8;
    public const int _model_start = 9;
    public const int _model_end = 10;
    public const int _model_ref_start = 11;
    public const int _ident = 12;
    public const int maxT = 21;

    const bool _T = true;
    const bool _x = false;
    const int minErrDist = 2;

    public Scanner scanner;
    public Errors errors;

    public Token t;    // last recognized token
    public Token la;   // lookahead token
    int errDist = minErrDist;

    private void ResolveDependency(string resolvedOutput, ref Model model, int numDependencies)
    {
        if (!_generalOutputs.ContainsKey(resolvedOutput))
        {
            _generalOutputs.Add(resolvedOutput, _generalOutputs.Count);
        }
        if (_dependencyMappings.ContainsKey(resolvedOutput))
        {
            int outputIdx = _generalOutputs[resolvedOutput];
            var dependendLgs = _dependencyMappings[resolvedOutput];
            foreach (var dependentLg in dependendLgs)
            {
                if (numDependencies > 0) continue;
                int dependencyCnter = 0;
                if (_lgDependencies.ContainsKey(dependentLg))
                {
                    dependencyCnter = _lgDependencies[dependentLg];
                    if (dependencyCnter - 1 <= 0)
                    {
                        // Logic gate has no more dependencies
                        _lgDependencies.Remove(dependentLg);
                        dependencyCnter = 0;
                    }
                    else
                    {
                        _lgDependencies[dependentLg]--;
                        dependencyCnter--;
                    }
                    int outputResolvedIdx = dependentLg.Inputs.IndexOf(resolvedOutput);
                    foreach (var soc in dependentLg.Socs)
                    {
                        int matchingIdx = soc.InputIdxs.IndexOf(outputResolvedIdx);
                        if (matchingIdx < 0) continue;
                        soc.InputIdxs[matchingIdx] = outputResolvedIdx;
                    }
                }
                //if (dependencyCnter == 0 && numDependencies == 0)
                if (dependencyCnter == 0)
                {
                    if (!_generalOutputs.ContainsKey(dependentLg.Output))
                    {
                        _generalOutputs.Add(dependentLg.Output, _generalOutputs.Count);
                        //outputIdx = _generalOutputs.Count - 1;
                        outputIdx = _generalOutputs[dependentLg.Output];
                        foreach (var soc in dependentLg.Socs)
                        {
                            soc.SetOutput(outputIdx);
                        }
                    }
                    model.LogicGates.Add(dependentLg);
                    foreach (var definedInput in dependentLg.Inputs)
                    {
                        if (!_generalInputs.ContainsKey(definedInput))
                        {
                            _generalInputs.Add(definedInput, _generalInputs.Count);
                        }
                    }
                    if (_dependencyMappings.ContainsKey(dependentLg.Output))
                    {
                        int currNumDependencies = _lgDependencies.ContainsKey(dependentLg) ? _lgDependencies[dependentLg] : 0;
                        ResolveDependency(dependentLg.Output, ref model, currNumDependencies);
                    }
                }
            }
            if (numDependencies == 0)
            {
                _dependencyMappings.Remove(resolvedOutput);
            }
        }
    }

    private ISet<string> _primaryInputSet;
    private Dictionary<string, int> _generalInputs;
    private ISet<string> _primaryOutputSet;
    private Dictionary<string, int> _generalOutputs;
    private ISet<string> _modelNamesSet;
    private ISet<string> _inputsToResolve;

    private IDictionary<LogicGate, int> _lgDependencies;
    private IDictionary<string, IList<LogicGate>> _dependencyMappings;
    public DependencyTree<Model> _modelDependencyTree;

    private IList<string> _primaryInputList;
    private IList<string> _primaryOutputList;

    private IDictionary<string, ISet<string>> _modelDependenciesDict;
    private IDictionary<string, IList<ModelReference>> _modelReferenceDict;


    public IList<Model> _models;
    /*------------------------------------------------------------------*/


    public Parser(Scanner scanner)
    {
        this.scanner = scanner;
        errors = new Errors();
    }

    void SynErr(int n)
    {
        if (errDist >= minErrDist) errors.SynErr(la.line, la.col, n);
        errDist = 0;
    }

    public void SemErr(string msg)
    {
        if (errDist >= minErrDist) errors.SemErr(t.line, t.col, msg);
        errDist = 0;
    }

    void Get()
    {
        for (; ; )
        {
            t = la;
            la = scanner.Scan();
            if (la.kind <= maxT) { ++errDist; break; }

            la = t;
        }
    }

    void Expect(int n)
    {
        if (la.kind == n) Get(); else { SynErr(n); }
    }

    bool StartOf(int s)
    {
        return set[s, la.kind];
    }

    void ExpectWeak(int n, int follow)
    {
        if (la.kind == n) Get();
        else
        {
            SynErr(n);
            while (!StartOf(follow)) Get();
        }
    }


    bool WeakSeparator(int n, int syFol, int repFol)
    {
        int kind = la.kind;
        if (kind == n) { Get(); return true; }
        else if (StartOf(repFol)) { return false; }
        else
        {
            SynErr(n);
            while (!(set[syFol, kind] || set[repFol, kind] || set[0, kind]))
            {
                Get();
                kind = la.kind;
            }
            return StartOf(syFol);
        }
    }


    void Model(out Model model)
    {
        string currModelName = "NONE"; bool inputsGiven = false; bool outputsGiven = false;
        if (la.kind == 9)
        {
            Get();
            Expect(4);
            Expect(13);
            Expect(1);
            Ident(out currModelName);
            if (_modelNamesSet.Contains(currModelName))
            {
                throw new Exception(ErrorMsgBuilder.DUPLICATE_MODEL);
            }
            _modelNamesSet.Add(currModelName);

        }
        if (la.kind == 6)
        {
            Inputs();
            inputsGiven = true;
            while (la.kind == 6)
            {
                Inputs();
            }
        }
        if (la.kind == 7)
        {
            Outputs();
            outputsGiven = true;
            while (la.kind == 7)
            {
                Outputs();
            }
        }
        model = new Model(currModelName, _primaryInputList.ToArray(), _primaryOutputList.ToArray());
        Command(ref model);
        while (la.kind == 8 || la.kind == 11)
        {
            Command(ref model);
        }
        if (_inputsToResolve.Count > 0)
        {
            if (model.PrimaryInputs.Length == 0)
            {
                model.PrimaryInputs = _inputsToResolve.ToArray();
                for (int i = 0; i < model.PrimaryInputs.Length; i++)
                {
                    model.GeneralInputs.Add(model.PrimaryInputs[i], i);
                }
            }
            else
            {
                foreach (var inputToResolve in _inputsToResolve)
                {
                    model.GeneralInputs.Add(inputToResolve, model.GeneralInputs.Count);
                }
            }
        }
        var depLogicGatesWithPInputs = new List<LogicGate>();
        string[] entriesToResolve = inputsGiven ? _inputsToResolve.ToArray() : model.PrimaryInputs;
        foreach (var entry in entriesToResolve)
        {
            if (_inputsToResolve.Contains(entry))
            {
                _inputsToResolve.Remove(entry);
            }
            if (_dependencyMappings.ContainsKey(entry))
            {
                IList<LogicGate> dpGates = _dependencyMappings[entry];
                foreach (var dpGate in dpGates)
                {
                    if (_lgDependencies[dpGate] - 1 == 0)
                    {
                        depLogicGatesWithPInputs.Add(dpGate);
                        foreach (var pInput in dpGate.Inputs)
                        {
                            if (!_generalInputs.ContainsKey(pInput))
                            {
                                _generalInputs.Add(pInput, _generalInputs.Count());
                            }
                        }
                        bool updateSocs = !_generalOutputs.ContainsKey(dpGate.Output);
                        ResolveDependency(dpGate.Output, ref model, 0);
                        _lgDependencies.Remove(dpGate);
                        if (updateSocs)
                        {
                            foreach (var soc in dpGate.Socs)
                            {
                                soc.SetOutput(_generalOutputs[dpGate.Output]);
                            }
                        }
                    }
                    else
                    {
                        _lgDependencies[dpGate]--;
                    }
                }
                _dependencyMappings.Remove(entry);
            }
        }
        depLogicGatesWithPInputs.AddRange(model.LogicGates);
        model.LogicGates = depLogicGatesWithPInputs;

        if (!outputsGiven)
        {
            var nonIOutputs = new HashSet<string>();
            foreach (var output in _generalOutputs)
            {
                if (_generalInputs.ContainsKey(output.Key)) continue;
                nonIOutputs.Add(output.Key);
            }
            model.PrimaryOutputs = nonIOutputs.ToArray();
        }

        if (la.kind == 10)
        {
            Get();
            Expect(4);
            Expect(14);
        }
        ISet<string> pInputSet = model.PrimaryInputs.ToHashSet();
        IEnumerable<string> additionalPInputs = _generalInputs.Keys.Where(gInput => !_generalOutputs.ContainsKey(gInput)
        && !pInputSet.Contains(gInput));
        if (additionalPInputs.Any())
        {
            string[] newPInputs = additionalPInputs.ToArray();
            string[] oldPInputArray = model.PrimaryInputs;
            Array.Resize(ref oldPInputArray, model.PrimaryInputs.Length + newPInputs.Length);
            newPInputs.CopyTo(oldPInputArray, model.PrimaryInputs.Length);
            model.PrimaryInputs = oldPInputArray;
        }
        model.GeneralInputs = new Dictionary<string, int>(_generalInputs);
        model.GeneralOutputs = new Dictionary<string, int>(_generalOutputs);

    }

    void Ident(out string output)
    {
        output = "";
        if (la.kind == 12)
        {
            Get();
            output = t.val;
            while (la.kind == 12)
            {
                Get();
                output += t.val;
            }
            if (la.kind == 2)
            {
                Get();
                output += t.val;
                Expect(12);
                output += t.val;
                while (la.kind == 12)
                {
                    Get();
                    output += t.val;
                }
                Expect(3);
                output += t.val;
            }
        }
        else if (la.kind == 2)
        {
            Get();
            output = t.val;
            Expect(12);
            output += t.val;
            while (la.kind == 12)
            {
                Get();
                output += t.val;
            }
            Expect(3);
            output += t.val;
        }
        else SynErr(22);
    }

    void Inputs()
    {
        int currIdx = _generalInputs.Count;
        string currIdent = "";
        Expect(6);
        Expect(4);
        Expect(15);
        Expect(1);
        Ident(out currIdent);
        if (_primaryInputSet.Contains(currIdent))
        {
            throw new Exception(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_INPUT, currIdent));
        }
        _primaryInputSet.Add(currIdent);
        _generalInputs.Add(currIdent, currIdx++);
        _primaryInputList.Add(currIdent);

        while (la.kind == 1)
        {
            Get();
            Ident(out currIdent);
            if (_primaryInputSet.Contains(currIdent))
            {
                throw new Exception(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_INPUT, currIdent));
            }
            _primaryInputSet.Add(currIdent);
            _generalInputs.Add(currIdent, currIdx++);
            _primaryInputList.Add(currIdent);

        }
    }

    void Outputs()
    {
        int currIdx = _generalOutputs.Count;
        string currIdent = "";
        Expect(7);
        Expect(4);
        Expect(16);
        Expect(1);
        Ident(out currIdent);
        if (_primaryOutputSet.Contains(currIdent))
        {
            throw new Exception(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_OUTPUT, currIdent));
        }
        _primaryOutputSet.Add(currIdent);
        _generalOutputs.Add(currIdent, currIdx++);
        _primaryOutputList.Add(currIdent);

        while (la.kind == 1)
        {
            Get();
            Ident(out currIdent);
            if (_primaryOutputSet.Contains(currIdent))
            {
                throw new Exception(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_OUTPUT, currIdent));
            }
            _primaryOutputSet.Add(currIdent);
            _generalOutputs.Add(currIdent, currIdx++);
            _primaryOutputList.Add(currIdent);

        }
    }

    void Command(ref Model model)
    {
        if (la.kind == 11)
        {
            Model_Reference(model.Name);
        }
        else if (la.kind == 8)
        {
            Logic_Gate(ref model);
        }
        else SynErr(23);
    }

    void Model_Reference(string currModelName)
    {
        string formalParamName; string actualParamName; string refModelName;
        ModelReference modelRef; int currIdx = _generalInputs.Count();
        Expect(11);
        Expect(4);
        Expect(17);
        Expect(1);
        Ident(out refModelName);
        Expect(1);
        modelRef = new ModelReference(refModelName);
        ParamMapping(out formalParamName, out actualParamName);
        modelRef.AddParamMapping(formalParamName, actualParamName);
        while (la.kind == 1)
        {
            Get();
            ParamMapping(out formalParamName, out actualParamName);
            modelRef.AddParamMapping(formalParamName, actualParamName);
        }
        if (!_modelDependenciesDict.ContainsKey(currModelName))
        {
            _modelDependenciesDict.Add(currModelName, new HashSet<string>());
        }
        _modelDependenciesDict[currModelName].Add(refModelName);
        if (_modelDependenciesDict.ContainsKey(refModelName)
        && _modelDependenciesDict[refModelName].Contains(currModelName))
        {
            throw new Exception(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.CYCLIC_MODEL_REFERENCE,
            new string[] { currModelName, refModelName }));
        }

        if (!_modelReferenceDict.ContainsKey(currModelName))
        {
            _modelReferenceDict.Add(currModelName, new List<ModelReference>());
        }
        _modelReferenceDict[currModelName].Add(modelRef);

    }

    void Logic_Gate(ref Model model)
    {
        ISet<string> currDefinedInputs = new HashSet<string>();
        string output;
        IList<int> inputIdxs = new List<int>();
        int outputIdx = -1;
        IList<SingleOutputCover> socs = new List<SingleOutputCover>();
        SingleOutputCover currSOC;
        LogicGate lg = new LogicGate();
        string currIdent;
        IList<string> definedInputs = new List<string>();
        IList<string> dependencies = new List<string>();

        Expect(8);
        Expect(4);
        Expect(19);
        Expect(1);
        Ident(out currIdent);
        if (!currDefinedInputs.Contains(currIdent) && !_generalInputs.ContainsKey(currIdent) && !_generalOutputs.ContainsKey(currIdent))
        {
            if (!_inputsToResolve.Contains(currIdent))
            {
                _inputsToResolve.Add(currIdent);
            }
        }
        if (!currDefinedInputs.Contains(currIdent))
        {
            currDefinedInputs.Add(currIdent);
            definedInputs.Add(currIdent);
        }
        output = currIdent;

        while (la.kind == 1)
        {
            Get();
            Ident(out currIdent);
            output = currIdent; // Temporary mark current input as output
            if (!currDefinedInputs.Contains(currIdent) && !_generalInputs.ContainsKey(currIdent) && !_generalOutputs.ContainsKey(currIdent))
            {
                if (!_inputsToResolve.Contains(currIdent))
                {
                    _inputsToResolve.Add(currIdent);
                }
            }
            if (!currDefinedInputs.Contains(currIdent))
            {
                currDefinedInputs.Add(currIdent);
                definedInputs.Add(currIdent);
            }

        }
        if (_inputsToResolve.Contains(output))
        {
            _inputsToResolve.Remove(output);
        }
        lg.Output = output;
        if (currDefinedInputs.Count > 1)
        {
            int cnt = 0;
            foreach (var input in definedInputs)
            {
                if (!input.Equals(output))
                {
                    inputIdxs.Add(_generalInputs.ContainsKey(input) ? _generalInputs[input] : -cnt);
                    cnt++;
                    lg.AddInput(input);
                    if (!_primaryInputSet.Contains(input) && !_generalOutputs.ContainsKey(input))
                    {
                        dependencies.Add(input);
                    }
                }
            }
        }
        if (dependencies.Count == 0 || _generalOutputs.ContainsKey(output))
        {
            if (!_generalOutputs.ContainsKey(output))
            {
                _generalOutputs.Add(output, _generalOutputs.Count);
            }
            outputIdx = _generalOutputs[output];
        }
        int[] inputIdxsArr = inputIdxs.ToArray();

        Single_Output_Cover(inputIdxsArr, outputIdx, out currSOC);
        socs.Add(currSOC);
        while (la.kind == 5)
        {
            Single_Output_Cover(inputIdxsArr, outputIdx, out currSOC);
            socs.Add(currSOC);
        }
        lg.Socs = socs;
        if (dependencies.Count > 0)
        {
            _lgDependencies.Add(lg, dependencies.Count);
            foreach (var dependency in dependencies)
            {
                if (!_dependencyMappings.ContainsKey(dependency))
                {
                    _dependencyMappings.Add(dependency, new List<LogicGate>());
                }
                _dependencyMappings[dependency].Add(lg);
            }
        }
        else
        {
            model.LogicGates.Add(lg);
        }
        if (dependencies.Count == 0 && definedInputs.Count > 1)
        {
            for (int i = 0; i < definedInputs.Count - 1; i++)
            {
                if (!_generalInputs.ContainsKey(definedInputs[i]))
                {
                    _generalInputs.Add(definedInputs[i], _generalInputs.Count);
                }
            }
        }
        if (dependencies.Count == 0)
        {
            ResolveDependency(output, ref model, dependencies.Count);
        }

    }

    void ParamMapping(out string formalParam, out string actualParamName)
    {
        Ident(out formalParam);
        Expect(18);
        Ident(out actualParamName);
    }

    void Single_Output_Cover(int[] inputIdxs, int outputIdx, out SingleOutputCover soc)
    {
        soc = new SingleOutputCover();
        int currIdx = 0; bool isNegated = false;
        int relativeInputIdx = 0;

        Expect(5);
        if (la.kind == 12)
        {
            Get();
        }
        else if (la.kind == 20)
        {
            Get();
        }
        else SynErr(24);
        switch (t.val)
        {
            case "-":
                break;
            case "1":
                isNegated = false;
                break;
            case "0":
                isNegated = true;
                break;
        }
        if (!t.val.Equals("-"))
        {
            soc.AddInput(currIdx, relativeInputIdx++, isNegated);
        }
        currIdx++;

        while (la.kind == 9)
        {
            Get();
            if (la.kind == 12)
            {
                Get();
            }
            else if (la.kind == 20)
            {
                Get();
            }
            else SynErr(25);
            switch (t.val)
            {
                case "-":
                    break;
                case "1":
                    isNegated = false;
                    break;
                case "0":
                    isNegated = true;
                    break;
            }
            if (currIdx > inputIdxs.Length)
            {
                throw new Exception(ErrorMsgBuilder.BuildMessage(
                ErrorMsgBuilder.TOO_MANY_ARGS, inputIdxs.Length
                ));
            }
            if (currIdx < inputIdxs.Length)
            {
                if (!t.val.Equals("-"))
                {
                    soc.AddInput(currIdx, relativeInputIdx++, isNegated);
                }
                currIdx++;
            }

        }
        Expect(1);
        Expect(12);
        switch (t.val)
        {
            case "0":
                soc.SetOutputOff();
                break;
            case "1":
                break;
            default:
                throw new Exception("For output only 1 (ON) and 0 (OFF) allowed");
        }
        soc.SetOutput(outputIdx);

    }

    void EquivChecker()
    {
        _primaryInputSet = new HashSet<string>();
        _primaryOutputSet = new HashSet<string>();
        _generalInputs = new Dictionary<string, int>();
        _generalOutputs = new Dictionary<string, int>();
        _inputsToResolve = new HashSet<string>();
        _modelDependenciesDict = new Dictionary<string, ISet<string>>();
        _modelDependencyTree = new DependencyTree<Model>();
        _modelNamesSet = new HashSet<string>();
        _lgDependencies = new Dictionary<LogicGate, int>();
        _dependencyMappings = new Dictionary<string, IList<LogicGate>>();
        _modelReferenceDict = new Dictionary<string, IList<ModelReference>>();
        _primaryInputList = new List<string>();
        _primaryOutputList = new List<string>();
        Model currModel;
        _models = new List<Model>();

        Model(out currModel);
        _models.Add(currModel);
        _inputsToResolve.Clear();
        _primaryInputSet.Clear(); _primaryOutputSet.Clear();
        _generalInputs.Clear(); _generalOutputs.Clear();
        _primaryInputList.Clear();
        _primaryOutputList.Clear();
        if (!_modelDependenciesDict.ContainsKey(currModel.Name))
        {
            _modelDependenciesDict.Add(currModel.Name, new HashSet<string>());
        }
        _modelDependencyTree.Insert(currModel.Name, currModel, _modelDependenciesDict[currModel.Name]);

        while (StartOf(1))
        {
            Model(out currModel);
            _models.Add(currModel);
            _inputsToResolve.Clear();
            _primaryInputSet.Clear(); _primaryOutputSet.Clear();
            _generalInputs.Clear(); _generalOutputs.Clear();
            _primaryInputList.Clear();
            _primaryOutputList.Clear();
            if (!_modelDependenciesDict.ContainsKey(currModel.Name))
            {
                _modelDependenciesDict.Add(currModel.Name, new HashSet<string>());
            }
            _modelDependencyTree.Insert(currModel.Name, currModel, _modelDependenciesDict[currModel.Name]);

        }
        TreeUnroller<Model> unroller = new TreeUnroller<Model>();
        List<string> unrolledModelNames = unroller.Unroll(_modelDependencyTree.DependencyNodesDict,
        _modelDependencyTree.Entities, _modelDependencyTree).Select(model => model.Name).ToList();

        IDictionary<string, Model> modelDict = _models.ToDictionary(model => model.Name);
        if (_modelReferenceDict.Count > 0)
        {
            _modelReferenceDict.AsParallel().ForAll(modelRefDictEntry =>
            {
                if (modelRefDictEntry.Value.Count == 0) return;
                var modelName = modelRefDictEntry.Key;
                var currentModel = modelDict[modelName];
                var currGeneralInputs = currentModel.GeneralInputs.Select(x => x.Key).ToHashSet();
                var currGeneralOutputs = currentModel.GeneralOutputs.Select(x => x.Key).ToHashSet();

                ISet<string> definedInputs = new HashSet<string>();
                IDictionary<string, string> definedInputMapping = new Dictionary<string, string>();
                IDictionary<string, string> definedOutputMappings = new Dictionary<string, string>();
                ISet<string> newPrimaryInputs = new HashSet<string>();
                ISet<string> newPrimaryOutputs = new HashSet<string>();
                ISet<string> currDefinedPrimaryInputs;
                ISet<string> currDefinedPrimaryOutputs;
                foreach (var modelReference in modelRefDictEntry.Value)
                {
                    if (!modelDict.ContainsKey(modelReference.ReferencedModelName))
                    {
                        // TODO: Throw error
                    }
                    var refModel = modelDict[modelReference.ReferencedModelName];
                    definedInputs.Clear();
                    definedInputMapping.Clear();
                    definedOutputMappings.Clear();
                    newPrimaryInputs.Clear();
                    newPrimaryOutputs.Clear();
                    foreach ((string formalParamKey, string actualParamKey) in modelReference.FormalActualList)
                    {
                        // Ignore formal parameters that are not defined in the referenced model
                        if (!refModel.GeneralInputs.ContainsKey(formalParamKey) && !refModel.GeneralOutputs.ContainsKey(formalParamKey)) continue;
                        if (refModel.GeneralOutputs.ContainsKey(formalParamKey))
                        {
                            definedOutputMappings.Add(formalParamKey, actualParamKey);
                        }
                        else
                        {
                            if (!definedInputs.Contains(formalParamKey))
                            {
                                definedInputs.Add(formalParamKey);
                                definedInputMapping.Add(formalParamKey, actualParamKey);
                            }
                        }
                    }
                    if (definedOutputMappings.Count == 0)
                    {
                        throw new Exception("");
                    }
                    int temporalVarNum = 0;
                    var matchingLogicGates = new List<LogicGate>();
                    foreach (var logicGate in refModel.LogicGates)
                    {
                        // Find logic gates of referenced model which have a subset of all the defined 
                        // inputs of the model reference as inputs
                        if (Enumerable.Intersect(logicGate.InputSet, definedInputs).Count()
                        == logicGate.InputSet.Count)
                        {
                            var actualDefinedOutput = logicGate.Output;
                            if (!definedOutputMappings.ContainsKey(actualDefinedOutput))
                            {
                                var newKey = "tmp" + temporalVarNum++;
                                definedOutputMappings.Add(actualDefinedOutput, newKey);
                                if (!definedInputs.Contains(actualDefinedOutput))
                                {
                                    definedInputs.Add(actualDefinedOutput);
                                    definedInputMapping.Add(actualDefinedOutput, newKey);
                                }
                            }
                        }
                        matchingLogicGates.Add(logicGate);
                    }
                    if (matchingLogicGates.Any())
                    {
                        string actualOutputName;
                        int actualOutputIdx;
                        int currNumGeneralOutputs = currentModel.GeneralOutputs.Count;
                        foreach (var matchingLogicGate in matchingLogicGates)
                        {
                            if (!definedOutputMappings.ContainsKey(matchingLogicGate.Output))
                            {
                                throw new Exception("TODO, no output mapping for formal output " + matchingLogicGate.Output);
                            }

                            actualOutputName = definedOutputMappings[matchingLogicGate.Output];
                            if (!currentModel.GeneralOutputs.ContainsKey(actualOutputName))
                            {
                                currentModel.GeneralOutputs.Add(actualOutputName, currNumGeneralOutputs++);
                            }
                            actualOutputIdx = actualOutputIdx = currentModel.GeneralOutputs[actualOutputName];
                            currentModel.LogicGates.Add(matchingLogicGate.Transform(definedInputMapping, actualOutputName, actualOutputIdx));
                        }
                        var newInputs = definedInputMapping.Where(kvMapping => !currentModel.GeneralInputs.ContainsKey(kvMapping.Value))
                    .Select(formalActualMapping => formalActualMapping.Value);
                        if (newInputs.Any())
                        {
                            var newIdx = currentModel.GeneralInputs.Count;
                            foreach (var newInput in newInputs)
                            {
                                currentModel.GeneralInputs.Add(newInput, newIdx++);
                            }
                        }
                    }
                }
                currDefinedPrimaryInputs = currentModel.PrimaryInputs.ToHashSet();
                currDefinedPrimaryOutputs = currentModel.PrimaryOutputs.ToHashSet();
                foreach (var output in currentModel.GeneralOutputs)
                {
                    if (currentModel.GeneralInputs.ContainsKey(output.Key)
                || currDefinedPrimaryOutputs.Contains(output.Key)) continue;
                    newPrimaryOutputs.Add(output.Key);
                }
                if (newPrimaryOutputs.Any())
                {
                    var currNumPrimaryOutputs = currDefinedPrimaryOutputs.Count();
                    var newPrimaryOutputsArr = new string[currNumPrimaryOutputs + newPrimaryOutputs.Count()];
                    currentModel.PrimaryOutputs.CopyTo(newPrimaryOutputsArr, 0);
                    newPrimaryOutputs.CopyTo(newPrimaryOutputsArr, currNumPrimaryOutputs);
                    currentModel.PrimaryOutputs = newPrimaryOutputsArr;
                }
                foreach (var input in currentModel.GeneralInputs)
                {
                    if (currentModel.GeneralOutputs.ContainsKey(input.Key)
                || currDefinedPrimaryInputs.Contains(input.Key)) continue;
                    newPrimaryInputs.Add(input.Key);
                }
                if (newPrimaryInputs.Any())
                {
                    var currNumPrimaryInputs = currDefinedPrimaryInputs.Count();
                    var newPrimaryInputsArr = new string[currNumPrimaryInputs + newPrimaryInputs.Count()];
                    currentModel.PrimaryInputs.CopyTo(newPrimaryInputsArr, 0);
                    newPrimaryInputs.CopyTo(newPrimaryInputsArr, currNumPrimaryInputs);
                    currentModel.PrimaryInputs = newPrimaryInputsArr;
                }
                modelDict[currentModel.Name] = currentModel;
            });
            _models = unrolledModelNames.Select(modelName => modelDict[modelName]).ToList();
        }

    }



    public void Parse()
    {
        la = new Token();
        la.val = "";
        Get();
        EquivChecker();
        Expect(0);

    }

    static readonly bool[,] set = {
        {_T,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x},
        {_x,_x,_x,_x, _x,_x,_T,_T, _T,_T,_x,_T, _x,_x,_x,_x, _x,_x,_x,_x, _x,_x,_x}

    };
} // end Parser


public class Errors
{
    public int count = 0;                                    // number of errors detected
    public System.IO.TextWriter errorStream = Console.Out;   // error messages go to this stream
    public string errMsgFormat = "-- line {0} col {1}: {2}"; // 0=line, 1=column, 2=text

    public virtual void SynErr(int line, int col, int n)
    {
        string s;
        switch (n)
        {
            case 0: s = "EOF expected"; break;
            case 1: s = "delimiter expected"; break;
            case 2: s = "o_bracket expected"; break;
            case 3: s = "c_bracket expected"; break;
            case 4: s = "prefix expected"; break;
            case 5: s = "single_cover_start expected"; break;
            case 6: s = "input_start expected"; break;
            case 7: s = "output_start expected"; break;
            case 8: s = "names_start expected"; break;
            case 9: s = "model_start expected"; break;
            case 10: s = "model_end expected"; break;
            case 11: s = "model_ref_start expected"; break;
            case 12: s = "ident expected"; break;
            case 13: s = "\"model\" expected"; break;
            case 14: s = "\"end\" expected"; break;
            case 15: s = "\"inputs\" expected"; break;
            case 16: s = "\"outputs\" expected"; break;
            case 17: s = "\"subckt\" expected"; break;
            case 18: s = "\"=\" expected"; break;
            case 19: s = "\"names\" expected"; break;
            case 20: s = "\"-\" expected"; break;
            case 21: s = "??? expected"; break;
            case 22: s = "invalid Ident"; break;
            case 23: s = "invalid Command"; break;
            case 24: s = "invalid Single_Output_Cover"; break;
            case 25: s = "invalid Single_Output_Cover"; break;

            default: s = "error " + n; break;
        }
        errorStream.WriteLine(errMsgFormat, line, col, s);
        count++;
    }

    public virtual void SemErr(int line, int col, string s)
    {
        errorStream.WriteLine(errMsgFormat, line, col, s);
        count++;
    }

    public virtual void SemErr(string s)
    {
        errorStream.WriteLine(s);
        count++;
    }

    public virtual void Warning(int line, int col, string s)
    {
        errorStream.WriteLine(errMsgFormat, line, col, s);
    }

    public virtual void Warning(string s)
    {
        errorStream.WriteLine(s);
    }
} // Errors


public class FatalError : Exception
{
    public FatalError(string m) : base(m) { }
}
