﻿using System;
using System.Collections.Generic;

namespace EquivCheckerCompGen.Entities
{
    public class ModelReference
    {
        public string ReferencedModelName { get; set; }
        public IDictionary<string, string> FormalActualList { get; set; }

        public ModelReference(string referencedModelname)
        {
            ReferencedModelName = referencedModelname;
            FormalActualList = new Dictionary<string, string>();
        }

        public void AddParamMapping(string formalParamName, string actualParamName)
        {
            if (string.IsNullOrWhiteSpace(formalParamName))
            {
                throw new ArgumentException(ErrorMsgBuilder.INVALID_FORMAL_PARAM_MAPPING);
            }
            if (string.IsNullOrWhiteSpace(actualParamName))
            {
                throw new ArgumentException(ErrorMsgBuilder.INVALID_FORMAL_PARAM_MAPPING);
            }
            if (!FormalActualList.TryAdd(formalParamName, actualParamName))
            {
                throw new ArgumentException(ErrorMsgBuilder.BuildMessage(ErrorMsgBuilder.DUPLICATE_FORMAL_PARAM, formalParamName));
            }
        }
    }
}
