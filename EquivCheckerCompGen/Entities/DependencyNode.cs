﻿using System;
using System.Collections.Generic;

namespace EquivCheckerCompGen.Entities
{
    public class DependencyNode
    {
        public string Key { get; set; }
        public IDictionary<string, DependencyNode> DependentNodes { get; set; }
        public ISet<string> Parents { get; set; }

        public DependencyNode(string key)
        {
            Key = key;
            DependentNodes = new Dictionary<string, DependencyNode>();
            Parents = new HashSet<string>();
        }

        public bool AddDependentNode(string key)
        {
            if (string.IsNullOrWhiteSpace(key)) throw new ArgumentException("Cannot insert entity with no key");
            DependencyNode newNode = new DependencyNode(key);
            if (DependentNodes.ContainsKey(key)) return false;
            newNode.AddParent(key);
            DependentNodes.Add(key, newNode);
            return true;
        }

        public bool AddDependentNode(DependencyNode childNode)
        {
            if (childNode == null) return false;
            if (DependentNodes.ContainsKey(childNode.Key)) return false;
            childNode.AddParent(Key);
            DependentNodes.Add(childNode.Key, childNode);
            return true;
        }

        public void AddParent(string parentKey)
        {
            if (Parents.Contains(parentKey)) return;
            Parents.Add(parentKey);
        }
    }
}
