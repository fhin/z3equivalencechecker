﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EquivCheckerCompGen.Entities
{
    public class SingleOutputCover
    {
        private int _outputIdx;

        public IList<int> InputIdxs { get; private set; }
        public int OutputIdx
        {
            get { return _outputIdx; }
            private set
            {
                //if (value < 0) throw new ArgumentException("Output idx must be greater than zero !");
                _outputIdx = value;
            }
        }
        public IList<int> NegatedInputsSet { get; private set; }
        public bool IsOutputOff { get; private set; }

        public SingleOutputCover()
        {
            InputIdxs = new List<int>();
            NegatedInputsSet = new List<int>();
            _outputIdx = -1;
            IsOutputOff = false;
        }

        public void AddInputs(IEnumerable<int> inputIdxs, ISet<int> negated)
        {
            if (inputIdxs == null || negated == null)
            {
                throw new ArgumentException("");
            }
            int cnt = 0;
            foreach (var input in inputIdxs)
            {
                AddInput(input, cnt, negated.Contains(cnt) ? true : false);
                cnt++;
            }
        }

        public void AddInput(int inputIdx, int relativeIdx, bool isNegated)
        {
            InputIdxs.Add(inputIdx);
            if (isNegated) NegatedInputsSet.Add(relativeIdx);
        }

        public void SetOutputOff()
        {
            IsOutputOff = true;
        }

        public void SetOutput(int outputIdx)
        {
            OutputIdx = outputIdx;
        }

        public override string ToString()
        {
            ISet<int> negatedInputs = NegatedInputsSet.ToHashSet();
            string output = "| ";
            foreach (var idx in InputIdxs)
            {
                output += negatedInputs.Contains(idx) ? "-" + idx.ToString() : idx.ToString();
                output += " | ";
            }
            output += " " + OutputIdx.ToString() + " |";
            return output;
        }
    }
}
