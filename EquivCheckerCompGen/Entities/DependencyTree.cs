﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EquivCheckerCompGen.Entities
{
    public class DependencyTree<T>
    {
        public IDictionary<string, DependencyNode> DependencyNodesDict { get; set; }
        public IDictionary<string, T> Entities { get; set; }
        private IDictionary<string, DependencyNode> NodeLookupDict { get; set; }
        private readonly object lockObj;
        private bool foundChildFlag;

        public DependencyTree()
        {
            DependencyNodesDict = new Dictionary<string, DependencyNode>();
            NodeLookupDict = new Dictionary<string, DependencyNode>();
            Entities = new Dictionary<string, T>();
            lockObj = new object();
            foundChildFlag = false;
        }

        public void Insert(string key, T entity, IEnumerable<string> parents)
        {
            foundChildFlag = false;
            if (parents == null || !parents.Any())
            {
                if (!DependencyNodesDict.ContainsKey(key))
                {
                    var newParentNode = new DependencyNode(key);
                    DependencyNodesDict.Add(key, newParentNode);
                    NodeLookupDict.Add(key, newParentNode);
                }
                UpdateNodeValue(key, entity);
            }
            foreach (var parentKey in parents)
            {
                Insert(key, parentKey, entity);
                /*
                if (!SearchAndReLink(parentKey, key, entity))
                {
                    if (!SearchAndInsert(DependencyNodesDict, parentKey, key, entity))
                    {
                        var parentNode = new DependencyNode(parentKey);
                        parentNode.AddDependentNode(key);
                        NodeLookupDict.Add(parentKey, parentNode);
                        DependencyNodesDict.Add(parentKey, parentNode);
                        UpdateNodeValue(key, entity);
                    }
                }
                foundChildFlag = false;
                */
                foundChildFlag = false;
            }
        }

        public void Insert(string key, string parentKey, T entity)
        {
            foundChildFlag = false;
            if (string.IsNullOrWhiteSpace(parentKey))
            {
                if (!DependencyNodesDict.ContainsKey(key))
                {
                    var newParentNode = new DependencyNode(key);
                    DependencyNodesDict.Add(key, newParentNode);
                    NodeLookupDict.Add(key, newParentNode);
                }
                UpdateNodeValue(key, entity);
            }
            else if (!SearchAndReLink(parentKey, key, entity))
            {
                if (!SearchAndInsert(DependencyNodesDict, parentKey, key, entity))
                {
                    DependencyNode parentNode;
                    if (NodeLookupDict.ContainsKey(parentKey))
                    {
                        parentNode = NodeLookupDict[parentKey];
                    }
                    else
                    {
                        parentNode = new DependencyNode(parentKey);
                    }
                    CreateLink(parentNode, key, entity);
                    DependencyNodesDict.Add(parentKey, parentNode);
                }
            }
            foundChildFlag = false;
        }

        public IEnumerable<T> Unroll()
        {
            BlockingCollection<T> unrolledTree = new BlockingCollection<T>();
            ISet<string> resolvedModels = new HashSet<string>();
            if (DependencyNodesDict.Count == 0) return unrolledTree;
            Parallel.ForEach(DependencyNodesDict, (kvPair) =>
            {
                var currNode = NodeLookupDict[kvPair.Key];
                bool needToUnroll = true;
                if (currNode.DependentNodes.Count == 0)
                {
                    lock (lockObj)
                    {
                        resolvedModels.Add(kvPair.Key);
                    }
                    unrolledTree.Add(Entities[kvPair.Key]);
                }
                lock (lockObj)
                {
                    if (!currNode.DependentNodes.Any(dependentNode => resolvedModels.Contains(dependentNode.Key)))
                    {
                        resolvedModels.Add(kvPair.Key);
                        needToUnroll = false;
                    }
                }
                if (needToUnroll)
                {

                }
            });
            return unrolledTree;
        }

        private bool SearchAndReLink(string parentKey, string key, T entity)
        {
            if (string.IsNullOrWhiteSpace(key) || entity == null || parentKey == null) return false;
            if (DependencyNodesDict.ContainsKey(parentKey) && DependencyNodesDict.ContainsKey(key))
            {
                CreateLink(DependencyNodesDict[parentKey], key, entity);
                return true;
            }
            return false;
        }

        private bool SearchAndInsert(IEnumerable<KeyValuePair<string, DependencyNode>> kvPairList, string parentKey, string key, T entity)
        {
            if (string.IsNullOrWhiteSpace(key) || entity == null || kvPairList == null) return false;
            Parallel.ForEach(
                kvPairList,
                () => { lock (lockObj) { return foundChildFlag; } },
                (kvPair, loopState, localSearchRes) =>
                {
                    if (localSearchRes)
                    {
                        loopState.Stop();
                    }
                    if (kvPair.Key.Equals(parentKey))
                    {
                        if (kvPair.Value.DependentNodes.ContainsKey(key))
                        {
                            throw new ArgumentException("TODO");
                        }
                        CreateLink(kvPair.Value, key, entity);
                        loopState.Stop();
                        return true;
                    }
                    else
                    {
                        return SearchAndInsert(kvPair.Value.DependentNodes, parentKey, key, entity);
                    }
                }, (localSearchRes) =>
                {
                    if (localSearchRes)
                    {
                        lock (lockObj)
                        {
                            foundChildFlag = true;
                        }
                    }
                });
            return foundChildFlag;
        }

        private void UpdateNodeValue(string key, T entity)
        {
            if (string.IsNullOrWhiteSpace(key)) return;
            if (!Entities.ContainsKey(key))
            {
                Entities.Add(key, entity);
            }
            else
            {
                Entities[key] = entity;
            }
        }

        private void UpdateLookupNode(string key, DependencyNode node)
        {
            if (string.IsNullOrWhiteSpace(key) || node == null) return;
            if (!NodeLookupDict.ContainsKey(key))
            {
                NodeLookupDict.Add(key, node);
            }
            else
            {
                NodeLookupDict[key] = node;
            }
        }

        private void CreateLink(DependencyNode parentNode, string childKey, T childEntity)
        {
            DependencyNode childNode;
            if (!DependencyNodesDict.ContainsKey(childKey))
            {
                childNode = new DependencyNode(childKey);
            }
            else
            {
                childNode = DependencyNodesDict[childKey];
                DependencyNodesDict.Remove(childKey);
            }
            UpdateNodeValue(childKey, childEntity);
            childNode.AddParent(parentNode.Key);
            UpdateLookupNode(childKey, childNode);

            parentNode.AddDependentNode(childNode);
            UpdateLookupNode(parentNode.Key, parentNode);
        }
    }
}
