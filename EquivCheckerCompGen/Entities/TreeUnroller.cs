﻿using System.Collections.Generic;
using System.Linq;

namespace EquivCheckerCompGen.Entities
{
    public class TreeUnroller<T>
    {
        private ISet<string> resolvedModels;
        private IDictionary<string, DependencyNode> nodeLookupDict;
        private IDictionary<string, T> entityLookupDict;
        private IDictionary<string, DependencyNode> waitingList;
        private IList<T> foundEntities;

        public TreeUnroller()
        {
            resolvedModels = new HashSet<string>();
            nodeLookupDict = new Dictionary<string, DependencyNode>();
            entityLookupDict = new Dictionary<string, T>();
            waitingList = new Dictionary<string, DependencyNode>();
            foundEntities = new List<T>();
        }

        public IEnumerable<T> Unroll(IDictionary<string, DependencyNode> nodeLookupDict,
            IDictionary<string, T> entityLookupDict, DependencyTree<T> tree)
        {
            this.nodeLookupDict = nodeLookupDict;
            this.entityLookupDict = entityLookupDict;

            Unroll(tree.DependencyNodesDict);
            return foundEntities;
        }

        private void Unroll(IEnumerable<KeyValuePair<string, DependencyNode>> nodes)
        {
            if (!nodes.Any()) return;
            foreach ((string nodeKey, DependencyNode node) in nodes)
            {
                if (!node.Parents.Any())
                {
                    resolvedModels.Add(nodeKey);
                    foundEntities.Add(entityLookupDict[nodeKey]);
                    Unroll(node.DependentNodes);
                }
                else if (node.Parents.Any(parentKey => !resolvedModels.Contains(parentKey)))
                {
                    if (!waitingList.ContainsKey(nodeKey))
                    {
                        waitingList.Add(nodeKey, node);
                    }
                    continue;
                }
                else
                {
                    resolvedModels.Add(nodeKey);
                    foundEntities.Add(entityLookupDict[nodeKey]);
                    if (waitingList.ContainsKey(nodeKey))
                    {
                        waitingList.Remove(nodeKey);
                    }
                    Unroll(node.DependentNodes);
                }
            }
        }
    }
}
