﻿using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EquivCheckerCompGen.Entities
{
    public class Model
    {
        public string Name { get; set; }
        public string[] PrimaryInputs { get; set; }
        public string[] PrimaryOutputs { get; set; }
        public IDictionary<string, int> GeneralInputs { get; set; }
        public IDictionary<string, int> GeneralOutputs { get; set; }
        public IList<LogicGate> LogicGates { get; set; }

        public Model(string modelName, string[] primaryInputs, string[] primaryOutputs)
        {
            Name = modelName;
            PrimaryInputs = primaryInputs;
            PrimaryOutputs = primaryOutputs;
            GeneralInputs = new Dictionary<string, int>();
            GeneralOutputs = new Dictionary<string, int>();
            LogicGates = new List<LogicGate>();
        }

        public override string ToString()
        {
            string stringRepresentation = "\n";
            stringRepresentation += "Model name: " + Name + "\n";
            stringRepresentation += "Primary inputs: " + string.Join(',', PrimaryInputs) + "\n";
            stringRepresentation += "Primary outputs: " + string.Join(',', PrimaryOutputs) + "\n";
            stringRepresentation += "Input mappings [name | idx]: \n";
            foreach (var kvPair in GeneralInputs)
            {
                stringRepresentation += "-> " + kvPair.Key + " | " + kvPair.Value + "\n";
            }
            stringRepresentation += "Output mappings [name | idx]: \n";
            foreach (var kvPair in GeneralOutputs)
            {
                stringRepresentation += "-> " + kvPair.Key + " | " + kvPair.Value + "\n";
            }
            stringRepresentation += "Logic Gates [row format: n-inputs given as relative indizes to inputs of logic gate " +
                "(first input of logic gate is negated [e.g. !0]) + output idx] \n";

            foreach (var lg in LogicGates)
            {
                string logicGateAsString = lg.ToString();
                int logicGateDelimiterLength = string.Join(',', lg.Inputs).Length + "Inputs: ".Length;
                stringRepresentation += Repeat("=", logicGateDelimiterLength) + "\n";
                stringRepresentation += lg.ToString();
                stringRepresentation += Repeat("=", logicGateDelimiterLength) + "\n";
            }
            return stringRepresentation;
        }

        private static string Repeat(string stringToRep, int numReps)
        {
            return new StringBuilder(numReps).Insert(0, stringToRep, numReps).ToString();
        }

        public IEnumerable<(ISet<string>, LogicGate)> GenerateLogicGateSignatures()
        {
            IList<(ISet<string>, LogicGate)> logicGateSignatures = new List<(ISet<string>, LogicGate)>();
            LogicGates.AsParallel().ForAll(logicGate =>
            {
                var signatureHashSet = logicGate.Inputs.ToHashSet();
                signatureHashSet.Add(logicGate.Output);
                logicGateSignatures.Add((signatureHashSet, logicGate));
            });
            return logicGateSignatures;
        }
    }
}
