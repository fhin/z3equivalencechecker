﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EquivCheckerCompGen.Entities
{
    public class LogicGate
    {
        private IList<string> _inputs;

        public IList<SingleOutputCover> Socs { get; set; }

        public IList<string> Inputs
        {
            get { return _inputs; }
            set
            {
                foreach (var input in value)
                {
                    if (!InputSet.Contains(input))
                    {
                        InputSet.Add(input);
                    }
                }
                _inputs = value;
            }
        }

        public ISet<string> InputSet { get; set; }

        public string Output { get; set; }

        public LogicGate()
        {
            Socs = new List<SingleOutputCover>();
            _inputs = new List<string>();
            InputSet = new HashSet<string>();
        }
        public override string ToString()
        {
            string output = "";
            output += "Inputs: " + string.Join(',', Inputs) + "\n";
            output += "Output: " + Output + "\n";
            foreach (var sg in Socs)
            {
                output += sg.ToString() + "\n";
            }
            return output;
        }

        public LogicGate Transform(IDictionary<string, string> formalActualDict, string newOutputName, int newOutputIdx)
        {
            if (formalActualDict.Count == 0) return this;
            if (string.IsNullOrWhiteSpace(newOutputName) || newOutputIdx < 0)
            {
                throw new ArgumentException("TODO");
            }
            LogicGate gate = new LogicGate
            {
                Inputs = new string[Inputs.Count],
                Output = newOutputName
            };
            foreach (var singleOutputCover in Socs)
            {
                var newSingleOutputCover = new SingleOutputCover();
                newSingleOutputCover.AddInputs(singleOutputCover.InputIdxs, singleOutputCover.NegatedInputsSet.ToHashSet());
                newSingleOutputCover.SetOutput(newOutputIdx);
                gate.Socs.Add(newSingleOutputCover);
            }
            gate.Inputs = Inputs.Where(formalInputName => formalActualDict.ContainsKey(formalInputName))
                .Select(formalInputName => formalActualDict[formalInputName]).ToList();
            if (gate.Inputs.Count != Inputs.Count)
            {
                throw new ArgumentException("TODO, could not transform logic gate");
            }
            gate.InputSet = gate.Inputs.ToHashSet();
            return gate;
        }

        public void AddInput(string input)
        {
            if (string.IsNullOrWhiteSpace(input)) return;
            if (!InputSet.Contains(input))
            {
                InputSet.Add(input);
                Inputs.Add(input);
            }
        }
    }
}
