﻿namespace EquivCheckerCompGen
{
    public class Constants
    {
        public static char DELIMITER = ';';
        public static char OPENING_B = '(';
        public static char CLOSING_B = ')';
        public static char OPENING_SB = '[';
        public static char CLOSING_SB = ']';
        public static char PREFIX = '.';
        public static char SINGLE_COVER_START = '>';
        public static char INPUT_START = '<';
        public static char OUTPUT_START = ':';
        public static char NAMES_START = '^';
        public static char MODEL_START = '!';
        public static char MODEL_END = '~';
        public static char UNDERSCORE = '_';
        public static char MODEL_REF_START = '?';
        public static char COMMENT_START_SYMB = '#';
    }
}
