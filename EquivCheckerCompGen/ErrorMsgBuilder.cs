﻿namespace EquivCheckerCompGen
{
    public class ErrorMsgBuilder
    {

        public static readonly string DUPLICATE_MODEL = "There already exists a model with the name {0}";
        public static readonly string DUPLICATE_INPUT = "There is already an input defined with the name {0}";
        public static readonly string INVALID_INPUT = "Defined input: {0} is not defined in the list of inputs";
        public static readonly string DUPLICATE_OUTPUT = "There is already an output defined with the name {0}";
        public static readonly string INVALID_OUTPUT = "Defined output: {0} is not defined in the list of outputs";
        public static readonly string TOO_MANY_ARGS = "Too many arguments defined for logic gate. Expected: {0}";
        public static readonly string UNRESOLVED_INPUTS = "There were unresolved inputs in the given model, namely: {0}";

        public static readonly string CYCLIC_MODEL_REFERENCE = "Cannot define a cyclic reference between the two models {0} and {1}";
        public static readonly string DUPLICATE_FORMAL_PARAM = "There is already a mapping defined for the given formal parameter {0} for the given model reference !";
        public static readonly string INVALID_FORMAL_PARAM_MAPPING = "Invalid model reference input mapping, expected format: <formalParamName>=<actualParamName>";

        public static readonly string PRIMARY_OUTPUT_CNT_MISSMATCH = "The two models have a different number of primary outputs, first model had {0} and the second on had {1}";
        public static readonly string EMPTY_LOGIC_GATE = "Could not create expression for empty gate !";
        public static string BuildMessage(string format, object args)
        {
            return string.Format(format, args);
        }

        public static string BuildMessage(string format, object[] args)
        {
            return string.Format(format, args);
        }
    }
}
